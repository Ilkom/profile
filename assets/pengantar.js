/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */

var BaseTableDatatables = function() {

    var initValidationUpdate = function() {
        jQuery('.form-update-pengantar').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },

            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            rules: {
                name: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                password: {
                    minlength: 5,
                },
                repassword: {
                    minlength: 5,
                    equalTo: '#password-update-pengantar',
                },
            },
            messages: {
                name: {
                    required: 'Please enter a name',
                },
                email: 'Please enter a valid email address',
                password: {
                    minlength: 'Your password must be at least 5 characters long',
                },
                repassword: {
                    minlength: 'Your password must be at least 5 characters long',
                    equalTo: 'Please enter the same password as above',
                },
            },
            submitHandler: function(form) {
                $form = $(form);
                var button = $form.find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('saving..');
                $.ajax({
                    url:$form.attr('action'),
                    type:'POST',
                    data:$form.serialize(),
                    success: function(res) {
                        if (res.data == '1') {
                            form.reset();

                            $('#modal-update-pengantar').modal('hide');
                            window.location.reload();
                        }else {
                            $('#modal-notif .block-content p').text(res.data);
                            $('#modal-notif').modal('show');
                        }
                        button.removeAttr('disabled');
                        button.text('Save');
                    },

                    error: function(jqXHR, exception) {
                        $('#modal-notif .block-content p').text(jqXHR.status);
                        $('#modal-notif').modal('show');
                        button.removeAttr('disabled');
                        button.text('Save');
                    },
                });
                return false; // required to block normal submit since you used ajax
            },
        });
    };

    return {
        init: function() {

            initValidationUpdate();

        },
    };
}();

// Initialize when page loads
jQuery(function() {
    BaseTableDatatables.init();
});

/* PROCCESSING */
// open modal add/update
function openModal(target, type, id) {
    if (type == 'update') {
        $.get('/kk/get/'+id, function(res) {
            var keluarga = res.data;
            $('.form-update-keluarga').attr('action', '/pengantar/update/'+id);
            $('#noKK-update-keluarga').val(keluarga.no_kk);
            $('#alamat-update-keluarga').val(keluarga.alamat);
            $('#desa-update-keluarga').val(keluarga.nama_desa);
            $('#rt-update-keluarga').val(keluarga.rt);
            $('#rw-update-keluarga').val(keluarga.rw);
        });
    }

    $(target).modal('show');
}
