/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */

var BaseTableDatatables = function() {
    var initValidationAdd = function() {
        jQuery('.form-add-organization').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },

            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            rules: {
                ImageFile: {
                    required: true,
                },
                org_name: {
                    required: true,
                },
                org_main_name: {
                    required: true,
                },
                org_address: {
                    required: true,
                },
                org_city: {
                    required: true,
                },
                org_phone_number: {
                    required: true,

                },
                org_fax_number: {
                    required: true,
                },
                org_email: {
                    required: true,
                },
            },
            messages: {
                ImageFile: {
                    required: 'pilih salah satu gambar',
                },
                org_name: {
                   required: 'Nama harus disisi',
                },
                org_main_name: {
                   required: 'Main Nama harus disisi',
                },
                org_address: {
                   required: 'Alamat harus disisi',
                },
                org_city: {
                   required: 'Kota harus disisi',
                },
                org_phone_number: {
                   required: 'Nomor HP/telpon harus disisi',
                },
                org_fax_number: {
                   required: 'Nomor Fax harus disisi',
                },
                org_email: {
                   required: 'Email harus disisi',
                },
            },
            submitHandler: function(form) {
                $form = $(form);
                var button = $form.find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('saving..');
                $.ajax({
                    url:$form.attr('action'),
                    type:'POST',
                    data:$form.serialize(),
                    success: function(res) {
                        if (res.data == '1') {
                            form.reset();
                            // $('#imgPreview').attr('src', '/img/default-paper.png');
                            // $('#modal-add-organization').modal('hide');
                            // $('#modal-succes .block-content p').text("Kegiatan Berhasil Disimpan");
                            // $('#modal-succes').modal('show');
                            alert('Data organisasi berhasil ditambahkan.');
                            window.location.href ='/admin/organization';

                        }else {
                            $('#modal-notif .block-content p').text(res.data);
                            $('#modal-notif').modal('show');
                        }
                        button.removeAttr('disabled');
                        button.text('Simpan Kegiatan');
                    },

                    error: function(jqXHR, exception) {
                      console.log(jqXHR)
                        $('#modal-notif .block-content p').text(jqXHR.responseJSON.data);
                        $('#modal-notif').modal('show');
                        button.removeAttr('disabled');
                        button.text('Simpan Kegiatan');;
                    },
                });
                return false; // required to block normal submit since you used ajax
            },
        });
    };

    var initValidationUpdate = function() {
        jQuery('.form-update-organization').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },

            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            submitHandler: function(form) {
                $form = $(form);
                var button = $form.find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('ubah..');
                $.ajax({
                    url:$form.attr('action'),
                    type:'POST',
                    data:$form.serialize(),
                    success: function(res) {
                        if (res.data == '1') {
                          alert('Data organisasi berhasil diubah.');
                          window.location.reload();
                          // $('#modal-succes .block-content p').text("Event Berhasil Diubah");
                          // $('#modal-succes').modal('show');
                        }else {
                            $('#modal-notif .block-content p').text(res.data);
                            $('#modal-notif').modal('show');
                        }
                        button.removeAttr('disabled');
                        button.text('Simpan Event');
                    },

                    error: function(jqXHR, exception) {
                        $('#modal-notif .block-content p').text(jqXHR.status);
                        $('#modal-notif').modal('show');
                        button.removeAttr('disabled');
                        button.text('Simpan Event');
                    },
                });
                return false; // required to block normal submit since you used ajax
            },
        });
    };

    return {
        init: function() {
            // Init Datatables
            initValidationAdd();
            initValidationUpdate();
        },
    };
}();

// Initialize when page loads
jQuery(function() {
    BaseTableDatatables.init();
});

/* PROCCESSING */
function openModal(target, type, id) {
    $(target).modal('show');
}

// add
$('input.upload').on('change', function(e) {
    if (this.files && this.files[0].name.match(/\.(jpg|jpeg|png|JPG|JPEG|PNG)$/)) {
        var image = $('#cropper-wrap-img > img'), cropBoxData, canvasData;
        var reader = new FileReader();
        reader.onload = function(e) {
            image.attr('src', e.target.result);
        };

        reader.readAsDataURL(this.files[0]);
        $('#cropper-modal').modal('show');
    }else {
        alert('file not supported');
    }
});

$('#cropper-modal').on('shown.bs.modal', function() {
    var image = $('#cropper-wrap-img > img'), cropBoxData, canvasData;
    image.cropper({
        aspectRatio: 1 / 1,
        autoCropArea: 0.5,
        cropBoxResizable: true,
        checkImageOrigin: true,
        responsive: true,
        built: function() {
            // Strict mode: set crop box data first
            image.cropper('setCropBoxData', cropBoxData);
            image.cropper('setCanvasData', canvasData);
        },
    });
});


$('.btn-crop').on('click', function(e) {
    var imgb64 = $('#cropper-wrap-img > img').cropper('getCroppedCanvas').toDataURL('image/png');
    $('img#imgPreview').attr('src', imgb64);
    $('#srcDataCrop').val(imgb64);
    $('img#imgPreview-update').attr('src', imgb64);
    $('#srcDataCrop-update').val(imgb64);
    $('#cropper-modal').modal('hide');
});

// edit
$('#cropper-modal').on('hidden.bs.modal', function() {
    $('#cropper-wrap-img > img').cropper('destroy');
    // $('body').addClass('modal-open');
});

$('#modal-add-employee').on('hidden.bs.modal', function() {
    $('img#imgPreview').attr('src', '/img/default.png');
    $('#srcDataCrop').val('');
});

$('#modal-update-employee').on('hidden.bs.modal', function() {
    $('img#imgPreview-update').attr('src', '/img/default.png');
    $('#srcDataCrop-update').val('');
});
