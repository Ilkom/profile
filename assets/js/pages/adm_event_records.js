/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */

var BaseTableDatatables = function() {
    // Init full DataTable, for more examples you can check out https://www.datatables.net/
    var initDataTableEventRecords = function() {
        window.tableEventRecords = jQuery('.data-eventrecord').dataTable({
            order: [[2, 'asc']],
            columnDefs: [{ orderable: false, targets: [0, 1, 3,4,5] }],
            pageLength: 10,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            processing: true,
            serverSide: false,
            ajax: '/admin/eventrecord/get',
            columns: [
                    { data: 'id',
                      render:function(data, type, row) {
                          var str = '<div class="checkbox-eventrecord">' +
                                        '<input type="checkbox" name="labId" value="' + data + '" data-toggle="tooltip" title="Select to remove">' +
                                  '</div>';
                          return str;
                      },
                    },
                    { data: 'eventRecordGallery',
                      render: function(data, type, row) {
                          var str;
                          if (data != '') {
                            $.each( data, function( i, v ) {
                                if (i == 0) {
                                  str = '<img src="' + v.ergal_photo + '" class="img-thumbnail img" width="150px" style="border-radius:5%">';
                                }
                            });
                          }else {
                            str = '<img src="/img/default-paper.png" class="img-thumbnail img" width="150px" style="border-radius:5%">';
                          }
                        return str;
                      },
                    },
                    { data: 'erec_title' },
                    { data: 'erec_date' ,
                    render: function(data, type, row) {
                        var date = moment(data).format("DD-MM-YYYY");
                        return date;
                      },
                    },
                    { data: 'erec_description' },
                    { data: 'id',
                      render: function(data, type, row) {
                          var str = '<button class="btn btn-default" onClick="openModal(\'#modal-update-eventrecord\', \'update\', \'' + data + '\')" data-toggle="tooltip" title="Ubah Rekaman Kegiatan"><i class="si si-pencil"></i> </button><a style="margin-left:5px;"href="/admin/event/gallery/'+data+'" class="pull-right btn btn-warning" data-toggle="tooltip" title="Galeri Kegiatan"><i class="si si-picture"></i> </a>';
                          return str;
                      },
                    },

                  ],
        }).on('draw.dt', function() {
            $(this).removeAttr('style');
            $('[data-toggle="tooltip"]').tooltip();
            $('.checkbox-eventrecord').shiftcheckbox({
                checkboxSelector: ':checkbox',
                selectAll: $('.checkbox-eventrecord-all'),
                ignoreClick: 'a',
                onChange: function(checked) {

                },
            });
        });
    };

    // DataTables Bootstrap integration
    var bsDataTables = function() {
        var $DataTable = jQuery.fn.dataTable;

        // Set the defaults for DataTables init
        jQuery.extend(true, $DataTable.defaults, {
            dom:
                '<\'row\'<\'col-sm-6\'l><\'col-sm-6\'f>>' +
                '<\'row\'<\'col-sm-12\'tr>>' +
                '<\'row\'<\'col-sm-6\'i><\'col-sm-6\'p>>',
            renderer: 'bootstrap',
            oLanguage: {
                sLengthMenu: '_MENU_',
                sInfo: 'Showing <strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>',
                oPaginate: {
                    sPrevious: '<i class="fa fa-angle-left"></i>',
                    sNext: '<i class="fa fa-angle-right"></i>',
                },
            },
        });

        // Default class modification
        jQuery.extend($DataTable.ext.classes, {
            sWrapper: 'dataTables_wrapper form-inline dt-bootstrap',
            sFilterInput: 'form-control',
            sLengthSelect: 'form-control',
        });

        // Bootstrap paging button renderer
        $DataTable.ext.renderer.pageButton.bootstrap = function(settings, host, idx, buttons, page, pages) {
            var api     = new $DataTable.Api(settings);
            var classes = settings.oClasses;
            var lang    = settings.oLanguage.oPaginate;
            var btnDisplay, btnClass;

            var attach = function(container, buttons) {
                var i, ien, node, button;
                var clickHandler = function(e) {
                    e.preventrecordDefault();
                    if (!jQuery(e.currentTarget).hasClass('disabled')) {
                        api.page(e.data.action).draw(false);
                    }
                };

                for (i = 0, ien = buttons.length; i < ien; i++) {
                    button = buttons[i];

                    if (jQuery.isArray(button)) {
                        attach(container, button);
                    } else {
                        btnDisplay = '';
                        btnClass = '';

                        switch (button) {
                            case 'ellipsis':
                                btnDisplay = '&hellip;';
                                btnClass = 'disabled';
                                break;

                            case 'first':
                                btnDisplay = lang.sFirst;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;

                            case 'previous':
                                btnDisplay = lang.sPrevious;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;

                            case 'next':
                                btnDisplay = lang.sNext;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;

                            case 'last':
                                btnDisplay = lang.sLast;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;

                            default:
                                btnDisplay = button + 1;
                                btnClass = page === button ?
                                        'active' : '';
                                break;
                        }

                        if (btnDisplay) {
                            node = jQuery('<li>', {
                                class: classes.sPageButton + ' ' + btnClass,
                                'aria-controls': settings.sTableId,
                                tabindex: settings.iTabIndex,
                                id: idx === 0 && typeof button === 'string' ?
                                        settings.sTableId + '_' + button :
                                        null,
                            })
                            .append(jQuery('<a>', {
                                    href: '#',
                                })
                                .html(btnDisplay)
                            )
                            .appendTo(container);

                            settings.oApi._fnBindAction(
                                node, {action: button}, clickHandler
                            );
                        }
                    }
                }
            };

            attach(
                jQuery(host).empty().html('<ul class="pagination"/>').children('ul'),
                buttons
            );
        };

        // TableTools Bootstrap compatibility - Required TableTools 2.1+
        if ($DataTable.TableTools) {
            // Set the classes that TableTools uses to something suitable for Bootstrap
            jQuery.extend(true, $DataTable.TableTools.classes, {
                container: 'DTTT btn-group',
                buttons: {
                    normal: 'btn btn-default',
                    disabled: 'disabled',
                },
                collection: {
                    container: 'DTTT_dropdown dropdown-menu',
                    buttons: {
                        normal: '',
                        disabled: 'disabled',
                    },
                },
                print: {
                    info: 'DTTT_print_info',
                },
                select: {
                    row: 'active',
                },
            });

            // Have the collection use a bootstrap compatible drop down
            jQuery.extend(true, $DataTable.TableTools.DEFAULTS.oTags, {
                collection: {
                    container: 'ul',
                    button: 'li',
                    liner: 'a',
                },
            });
        }
    };

    var initValidationAdd = function() {
        jQuery('.form-add-eventrecord').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },

            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            rules: {
                erec_title: {
                    required: true,
                },
                erec_description: {
                    required: true,
                },
            },
            messages: {
                erec_title: {
                    required: 'Nama harus disisi',
                },
                erec_description: {
                    required: 'Deskripsi harus disisi',
                },
            },
            submitHandler: function(form) {
                $form = $(form);
                var button = $form.find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('saving..');
                $.ajax({
                    url:$form.attr('action'),
                    type:'POST',
                    data:$form.serialize(),
                    success: function(res) {
                        if (res.data == '1') {
                            form.reset();
                            $('#imgPreview').attr('src', '/img/default-paper.png');
                            $('#modal-add-eventrecord').modal('hide');
                            // $('#modal-succes .block-content p').text("Kegiatan Berhasil Disimpan");
                            // $('#modal-succes').modal('show');
                            window.tableEventRecords.api().ajax.reload();

                        }else {
                            $('#modal-notif .block-content p').text(res.data);
                            $('#modal-notif').modal('show');
                        }
                        button.removeAttr('disabled');
                        button.text('Simpan Kegiatan');
                    },

                    error: function(jqXHR, exception) {
                      console.log(jqXHR)
                        $('#modal-notif .block-content p').text(jqXHR.responseJSON.data);
                        $('#modal-notif').modal('show');
                        button.removeAttr('disabled');
                        button.text('Simpan Kegiatan');;
                    },
                });
                return false; // required to block normal submit since you used ajax
            },
        });
    };

    var initValidationUpdate = function() {
        jQuery('.form-update-eventrecord').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },

            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            submitHandler: function(form) {
                $form = $(form);
                var button = $form.find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('ubah..');
                $.ajax({
                    url:$form.attr('action'),
                    type:'POST',
                    data:$form.serialize(),
                    success: function(res) {
                        if (res.data == '1') {
                          $('#modal-update-eventrecord').modal('hide');
                          window.tableEventRecords.api().ajax.reload();
                          // $('#modal-succes .block-content p').text("EventRecords Berhasil Diubah");
                          // $('#modal-succes').modal('show');
                        }else {
                            $('#modal-notif .block-content p').text(res.data);
                            $('#modal-notif').modal('show');
                        }
                        button.removeAttr('disabled');
                        button.text('Simpan Labolatorium');
                    },

                    error: function(jqXHR, exception) {
                        $('#modal-notif .block-content p').text(jqXHR.status);
                        $('#modal-notif').modal('show');
                        button.removeAttr('disabled');
                        button.text('Simpan Labolatorium');
                    },
                });
                return false; // required to block normal submit since you used ajax
            },
        });
    };

    return {
        init: function() {
            // Init Datatables
            initValidationAdd();
            initValidationUpdate();
            bsDataTables();
            initDataTableEventRecords();
        },
    };
}();

// Initialize when page loads
jQuery(function() {
    BaseTableDatatables.init();
});

/* PROCCESSING */
// open modal
function openModal(target, type, id) {
    if (type == 'update') {
        $.get('/admin/eventrecord/get/' + id, function(res) {
            var eventrecord = res.data;
            $('.form-update-eventrecord').attr('action', '/admin/eventrecord/p/update/' + id);
            $('#title-update-eventrecord').val(eventrecord.erec_title);
            var date = moment(eventrecord.erec_date).format("YYYY-MM-DD");
            $('#date-update-eventrecord').val(date);
            $('#description-update-eventrecord').val(eventrecord.erec_description);
        });
    }
    $(target).modal('show');
}

// add
$('input.upload').on('change', function(e) {
    if (this.files && this.files[0].name.match(/\.(jpg|jpeg|png|JPG|JPEG|PNG)$/)) {
        var image = $('#cropper-wrap-img > img'), cropBoxData, canvasData;
        var reader = new FileReader();
        reader.onload = function(e) {
            image.attr('src', e.target.result);
        };

        reader.readAsDataURL(this.files[0]);
        $('#cropper-modal').modal('show');
    }else {
        alert('file not supported');
    }
});

$('#cropper-modal').on('shown.bs.modal', function() {
    var image = $('#cropper-wrap-img > img'), cropBoxData, canvasData;
    image.cropper({
        aspectRatio: 1 / 1,
        autoCropArea: 0.5,
        cropBoxResizable: true,
        checkImageOrigin: true,
        responsive: true,
        built: function() {
            // Strict mode: set crop box data first
            image.cropper('setCropBoxData', cropBoxData);
            image.cropper('setCanvasData', canvasData);
        },
    });
});


$('.btn-crop').on('click', function(e) {
    var imgb64 = $('#cropper-wrap-img > img').cropper('getCroppedCanvas').toDataURL('image/jpeg');
    $('img#imgPreview').attr('src', imgb64);
    $('#srcDataCrop').val(imgb64);
    $('img#imgPreview-update').attr('src', imgb64);
    $('#srcDataCrop-update').val(imgb64);
    $('#cropper-modal').modal('hide');
});

// edit
$('#cropper-modal').on('hidden.bs.modal', function() {
    $('#cropper-wrap-img > img').cropper('destroy');
    $('body').addClass('modal-open');
});

$('#modal-add-employee').on('hidden.bs.modal', function() {
    $('img#imgPreview').attr('src', '/img/default.png');
    $('#srcDataCrop').val('');
});

$('#modal-update-employee').on('hidden.bs.modal', function() {
    $('img#imgPreview-update').attr('src', '/img/default.png');
    $('#srcDataCrop-update').val('');
});

// remove
$(document).on('change', '.data-eventrecord input:checkbox', function() {
    if ($('.data-eventrecord input:checkbox:checked').length > 0) {
        $('.btn-delete-eventrecord').removeAttr('disabled');
    }else {
        $('.btn-delete-eventrecord').attr('disabled', 'disabled');
    }
});

$('.btn-delete-eventrecord').click(function(e) {
    if ($('.data-eventrecord input:checkbox:checked').length > 0) {
        var conf = confirm('Hapus Data EventRecords yang dipilih ?');
        if (conf) {
            var data = $('.data-eventrecord input:checkbox:checked').serialize();

            $.ajax({
                url:'/admin/eventrecord/p/remove',
                type:'POST',
                data:data,
                success: function(res) {
                    if (res.data == '1') {
                        window.tableEventRecords.api().ajax.reload();
                        $('.btn-delete-eventrecord').attr('disabled', 'disabled');
                        $('.data-eventrecord input:checkbox:checked').removeAttr('checked');
                    }else {
                        $('#modal-notif .block-content p').text(res.data);
                        $('#modal-notif').modal('show');
                    }
                },

                error: function(jqXHR, exception) {
                    $('#modal-notif .block-content p').text(jqXHR.status);
                    $('#modal-notif').modal('show');
                },
            });
        }
    }else {
        alert('tolong pilih salah satu eventrecord untuk menghapus!');
    }
});
