'use strict';

function createLecturerEdu(table) {
    table.increments('id').primary();
    table.string('ledu_name');
    table.string('ledu_year');
    table.string('ledu_city');
    table.text('ledu_description');
    table.integer('ledu_lecturer_id').references('lecturers.id');
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('lecturer_educations', createLecturerEdu),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('lecturer_educations'),
    ]);
};
