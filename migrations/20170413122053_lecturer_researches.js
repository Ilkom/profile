'use strict';

function createLecturerRes(table) {
    table.increments('id').primary();
    table.string('lres_name');
    table.string('lres_organizer',225);
    table.string('lres_research_url');
    table.string('lres_year');
    table.text('lres_description');
    table.integer('lres_lecturer_id').references('lecturers.id');
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('lecturer_researches', createLecturerRes),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('lecturer_researches'),
    ]);
};
