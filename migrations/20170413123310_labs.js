'use strict';

function createLabs(table) {
    table.increments('id').primary();
    table.string('lab_name', 100);
    table.text('lab_description');
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('labs', createLabs),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('labs'),
    ]);
};
