'use strict';

function createLecturerAch(table) {
    table.increments('id').primary();
    table.string('lach_name');
    table.string('lach_organizer');
    table.string('lach_year');
    table.text('lach_description');
    table.integer('lach_lecturer_id').references('lecturers.id');
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('lecturer_achievement', createLecturerAch),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('lecturer_achievement'),
    ]);
};
