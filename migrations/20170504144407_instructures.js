'use strict';

function createinstructures(table) {
    table.increments('id').primary();
    table.string('ins_nip', 100);
    table.string('ins_name', 100);
    table.string('ins_nickname', 100);
    table.string('ins_photo', 225);
    table.string('ins_gender', 50);
    table.string('ins_birthplace', 100);
    table.date('ins_birthdate');
    table.string('ins_handphone_number', 50);
    table.string('ins_email');
    table.string('ins_address');
    table.string('ins_city', 50);
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('instructures', createinstructures),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('instructures'),
    ]);
};
