'use strict';

function createLCMaterials(table) {
    table.increments('id').primary();
    table.string('lcm_name', 255);
    table.string('lcm_url', 255);
    table.text('lcm_description');
    table.integer('lcm_lc_id').references('lecturer_courses.id');
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('lc_materials', createLCMaterials),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('lc_materials'),
    ]);
};
