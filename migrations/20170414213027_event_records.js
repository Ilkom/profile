'use strict';

function createEventRecords(table) {
    table.increments('id').primary();
    table.string('erec_title',100);
    table.text('erec_description');
    table.date('erec_date');
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('event_records', createEventRecords),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('event_records'),
    ]);
};
