'use strict';

function createLectureCourses(table) {
    table.increments('id').primary();
    table.integer('lc_lecturer_id').references('lecturers.id');
    table.integer('lc_course_id').references('courses.id');
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('lecturer_courses', createLectureCourses),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('lecturer_courses'),
    ]);
};
