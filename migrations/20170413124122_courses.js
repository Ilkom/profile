'use strict';

function createCourses(table) {
    table.increments('id').primary();
    table.string('cou_name', 100);
    table.string('cou_semester', 50);
    table.text('cou_description');
    table.integer('cou_sks');
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('courses', createCourses),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('courses'),
    ]);
};
