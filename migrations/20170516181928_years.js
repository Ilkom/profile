'use strict';

function createYears(table) {
    table.increments('id').primary();
    table.string('y_year',50);
    table.text('y_semester');
    table.text('y_description');
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('academic_year', createYears),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('academic_year'),
    ]);
};
