'use strict';

function createLeaders(table) {
    table.increments('id').primary();
    table.string('lea_nip', 50);
    table.string('lea_name', 100);
    table.string('lea_nickname', 100);
    table.string('lea_photo');
    table.string('lea_gender', 20);
    table.string('lea_birthplace', 100);
    table.date('lea_birthdate');
    table.string('lea_handphone_number', 20);
    table.string('lea_email',100);
    table.string('lea_address');
    table.string('lea_city',100);
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('leaders', createLeaders),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('leaders'),
    ]);
};
