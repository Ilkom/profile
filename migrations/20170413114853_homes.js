'use strict';

function createHome(table) {
    table.increments('hom_id').primary();
    table.string('hom_background_img');
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('homes', createHome),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('homes'),
    ]);
};
