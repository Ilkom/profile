'use strict';

function createNews(table) {
    table.increments('new_id').primary();
    table.string('new_title');
    table.text('new_content');
    table.string('new_author');
    table.string('new_cover');
    table.date('new_date');
    // table.integer('desa_id').references('desa.id');
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('news', createNews),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('news'),
    ]);
};
