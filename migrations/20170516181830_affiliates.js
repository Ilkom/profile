'use strict';

function createAffiliates(table) {
    table.increments('id').primary();
    table.string('aff_partners_name',255);
    table.text('aff_logo');
    table.text('aff_description');
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('affiliates', createAffiliates),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('affiliates'),
    ]);
};
