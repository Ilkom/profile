'use strict';

function createLecturerExp(table) {
    table.increments('id').primary();
    table.string('le_title',255);
    table.text('le_description');
    table.integer('le_lecturer_id').references('lecturers.id');
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('lecturer_expertises', createLecturerExp),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('lecturer_expertises'),
    ]);
};
