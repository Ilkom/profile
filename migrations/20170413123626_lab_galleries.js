'use strict';

function createLabGal(table) {
    table.increments('lgal_id').primary();
    table.string('lgal_photo');
    table.text('lgal_description');
    table.integer('lgal_visibility');
    table.integer('lgal_lab_id').references('labs.lab_id');
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('lab_galleries', createLabGal),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('lab_galleries'),
    ]);
};
