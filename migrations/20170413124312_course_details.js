'use strict';

function createCourseDetails(table) {
    table.increments('cdet_id').primary();
    table.string('cdet_material', 100);
    table.string('cdet_url');
    table.string('cdet_url_word');
    table.string('cdet_url_ppt');
    table.string('cdet_url_pdf');
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('course_details', createCourseDetails),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('course_details'),
    ]);
};
