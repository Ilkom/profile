'use strict';

function createEventRecordGal(table) {
    table.increments('ergal_id').primary();
    table.string('ergal_photo');
    table.text('ergal_description');
    table.integer('ergal_visibility');
    table.integer('ergal_event_record_id').references('event_records.id');
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('event_record_galleries', createEventRecordGal),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('event_record_galleries'),
    ]);
};
