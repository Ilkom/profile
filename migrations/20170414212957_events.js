'use strict';

function createEvent(table) {
    table.increments('id').primary();
    table.string('eve_title',100);
    table.text('eve_description');
    table.string('eve_photo');
    table.date('eve_date');
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('events', createEvent),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('events'),
    ]);
};
