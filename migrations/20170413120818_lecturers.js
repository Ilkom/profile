'use strict';

function createLecturers(table) {
    table.increments('id').primary();
    table.string('lec_nip');
    table.string('lec_name');
    table.string('lec_nickname');
    table.string('lec_photo');
    table.string('lec_gender');
    table.string('lec_birthplace');
    table.date('lec_birthdate');
    table.string('lec_handphone_number');
    table.string('lec_email');
    table.string('lec_address');
    table.string('lec_city');
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('lecturers', createLecturers),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('lecturers'),
    ]);
};
