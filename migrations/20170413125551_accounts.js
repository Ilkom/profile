'use strict';

function createAccounts(table) {
    table.increments('id').primary();
    table.string('acc_email', 100);
    table.string('acc_password');
    table.string('acc_user_right', 100);
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('accounts', createAccounts),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('accounts'),
    ]);
};
