'use strict';

function createOrganization(table) {
    table.increments('org_id').primary();
    table.string('org_name', 100);
    table.string('org_logo');
    table.string('org_main_name', 100);
    table.string('org_main_logo');
    table.string('org_address');
    table.string('org_city', 100);
    table.string('org_phone_number', 20);
    table.string('org_fax_number',20);
    table.string('org_email',100);
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('organizations', createOrganization),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('organizations'),
    ]);
};
