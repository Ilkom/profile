'use strict';

function createCal(table) {
    table.increments('id').primary();
    table.date('acc_date');
    table.text('acc_event');
    table.text('acc_description');
    table.integer('acc_year_id').references('academic_year.id');
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('academic_calendars', createCal),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('academic_calendars'),
    ]);
};
