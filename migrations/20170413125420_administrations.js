'use strict';

function createAdministrations(table) {
    table.increments('id').primary();
    table.string('adm_nip', 50);
    table.string('adm_name', 100);
    table.string('adm_nickname', 100);
    table.string('adm_photo');
    table.string('adm_gender', 20);
    table.string('adm_birthplace', 100);
    table.date('adm_birthdate');
    table.string('adm_handphone_number', 20);
    table.string('adm_email',100);
    table.string('adm_address');
    table.string('adm_city',100);
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('administrations', createAdministrations),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('administrations'),
    ]);
};
