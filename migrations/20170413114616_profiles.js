'use strict';

function createProfile(table) {
    table.increments('pro_id').primary();
    table.text('pro_intro');
    table.text('pro_visions');
    table.text('pro_mission');
    table.text('pro_structure');
    table.string('pro_structure_img');
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('profiles', createProfile),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('profiles'),
    ]);
};
