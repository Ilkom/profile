'use strict';

const http = require('http');

module.exports = () => {

    // Craft response.
    function craft(data, message, status) {
        status = status || 200;

        const response = {
            meta: {
                code: status,
                message: message || http.STATUS_CODES[status],
            },
        };

        if (data != null) response.data = data;

        return response;
    };

    // Before routes
    app.use((req, res, next) => {
        req.locale = req.session.locale || config.i18n.defaultLocale;
        req.api = req.xhr || req.get('Accept').indexOf('html') < 0;

        // Base model only if request is not API.
        if (!req.api) {
            res.model = {
                user: req.user,
                locale: req.locale,
                version: version,

            };
        }

        res.craft = craft;

        res.ok = (data, message, status) => {
            status = status || 200;

            res.status(status).send(craft(data, message, status));
        };

        next();
    });
    //
    // Global setting desa
    // app.use(Async.route(function *(req, res, next) {
    //    var Desa =  yield Datadesa.forge().fetch().catch(console.error);
    //    res.model.desa = Desa.toJSON();
    //    next();
    //   }));


      // //  setting theme
      // app.use(Async.route(function *(req, res, next) {
      //    var Theme =  yield Settings.forge().fetch().catch(console.error);
      //    res.model.Theme = Theme.toJSON();
      //    next();
      //   }));

    //
      //  setting theme
      // app.use(Async.route(function *(req, res, next) {
      //    var Theme =  yield Setting.forge().fetch().catch(console.error);
      //    res.model.Theme = Theme.toJSON();
      //    next();
      //   }));


    // routes
    app.use('/', require('./web'));
    app.use('/api/v1', require('./api'));
    app.use('/test', require('./test'));

    // app.use(Async.route(function *(req, res, next) {
    //    var Desa =  yield Datadesa.forge().fetch().catch(console.error);
    //    res.model.desa = Desa.toJSON();
    //    next();
    //   }));

    // 404 handler
    app.use((req, res, next) => {
        const err = new Error('Not Found');
        err.status = 404;
        next(err);
    });



    // error handler
    app.use((err, req, res, next) => {
        const errSplitted = err.stack.split('\n');
        console.error({
            message: errSplitted[0],
            location: errSplitted[1]
                        .replace(config.appDir, '')
                        .replace(/\\/g, '/')
                        .trim(),
            url: req.originalUrl,
        });

        if (err.message === 'Invalid login data') {
            req.logout();
            return res.redirect('/');
        }

        const status = err.status || 500;

        if (req.api) {
            return res.status(status).send(craft(err.data, err.message, status));
        } else {
            const model = Object.assign({}, res.model, craft(err.data, err.message, status));
            return res.render('errors/error', model);
        };

    });





};
