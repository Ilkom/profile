
const express = require('express');
const router = express.Router();

var auth = require('../middlewares/authenticated');
var multipart = require('../middlewares/multipart');

// Index
// router.get('/print/:id', PersuratanController.print);


// backend End
router.get('/admin', AuthenticationController.index);
router.post('/admin/auth/login', AuthenticationController.login);
router.get('/admin/auth/logout', AuthenticationController.logout);

// dashboard
router.get('/admin/dashboard',auth.isAuthenticated(),DashboardController.index);

// profile
router.get('/admin/account/profile', auth.isAuthenticated(), AccountController.profile);
router.post('/admin/profile/password/p/update', auth.isAuthenticated(),AccountController.resetPass);

// administration & account
router.get('/admin/administration', auth.isAuthenticated(), AdministrationController.index);
router.get('/admin/administration/get', auth.isAuthenticated(), AdministrationController.get);
router.post('/admin/administration/p/add', auth.isAuthenticated(), AdministrationController.add);
router.post('/admin/administration/p/remove', auth.isAuthenticated(), AdministrationController.remove);
router.get('/admin/administration/get/:id', auth.isAuthenticated(),  AdministrationController.dataSingle);
router.post('/admin/administration/p/update/:id', auth.isAuthenticated(), AdministrationController.update);

// leaders / prodi
router.get('/admin/leaders', auth.isAuthenticated(), LeaderController.index);
router.get('/admin/leaders/get', auth.isAuthenticated(), LeaderController.get);
router.post('/admin/leaders/p/add', auth.isAuthenticated(), LeaderController.add);
router.post('/admin/leaders/p/remove', auth.isAuthenticated(), LeaderController.remove);
router.get('/admin/leaders/get/:id', auth.isAuthenticated(),  LeaderController.dataSingle);
router.post('/admin/leaders/p/update/:id', auth.isAuthenticated(), LeaderController.update);

// leacture
router.get('/admin/lecturers', auth.isAuthenticated(), LecturerController.index);
router.get('/admin/lecturers/get', auth.isAuthenticated(), LecturerController.get);
router.post('/admin/lecturers/p/add', auth.isAuthenticated(), LecturerController.add);
router.post('/admin/lecturers/p/remove', auth.isAuthenticated(), LecturerController.remove);
router.get('/admin/lecturers/get/:id', auth.isAuthenticated(),  LecturerController.dataSingle);
router.post('/admin/lecturers/p/update/:id', auth.isAuthenticated(), LecturerController.update);

// leacturer Education
router.get('/admin/lecturer/education/:id', auth.isAuthenticated(), LecturerEducationController.index);
router.get('/admin/lecturer/education/get/all/:lectureId', auth.isAuthenticated(), LecturerEducationController.get);
router.post('/admin/lecturer/education/p/add', auth.isAuthenticated(), LecturerEducationController.add);
router.post('/admin/lecturer/education/p/remove', auth.isAuthenticated(), LecturerEducationController.remove);
router.get('/admin/lecturer/education/get/:id', auth.isAuthenticated(),  LecturerEducationController.dataSingle);
router.post('/admin/lecturer/education/p/update/:id', auth.isAuthenticated(), LecturerEducationController.update);

// leacturer Expertises
router.get('/admin/lecturer/expertises/:id', auth.isAuthenticated(), LecturerExpertisesController.index);
router.get('/admin/lecturer/expertises/get/all/:lectureId', auth.isAuthenticated(), LecturerExpertisesController.get);
router.post('/admin/lecturer/expertises/p/add', auth.isAuthenticated(), LecturerExpertisesController.add);
router.post('/admin/lecturer/expertises/p/remove', auth.isAuthenticated(), LecturerExpertisesController.remove);
router.get('/admin/lecturer/expertises/get/:id', auth.isAuthenticated(),  LecturerExpertisesController.dataSingle);
router.post('/admin/lecturer/expertises/p/update/:id', auth.isAuthenticated(), LecturerExpertisesController.update);

// leacturer achievement
router.get('/admin/achievement', auth.isAuthenticated(), LecturerAchievementController.achievement);
router.get('/admin/lecturer/achievement/:id', auth.isAuthenticated(), LecturerAchievementController.index);
router.get('/admin/lecturer/achievement/get/all', auth.isAuthenticated(), LecturerAchievementController.getAll);
router.get('/admin/lecturer/achievement/get/all/:lectureId', auth.isAuthenticated(), LecturerAchievementController.get);
router.post('/admin/lecturer/achievement/p/add', auth.isAuthenticated(), LecturerAchievementController.add);
router.post('/admin/lecturer/achievement/p/remove', auth.isAuthenticated(), LecturerAchievementController.remove);
router.get('/admin/lecturer/achievement/get/:id', auth.isAuthenticated(),  LecturerAchievementController.dataSingle);
router.post('/admin/lecturer/achievement/p/update/:id', auth.isAuthenticated(), LecturerAchievementController.update);

// leacturer researche
router.get('/admin/researche', auth.isAuthenticated(), LecturerResearcheController.researche);
router.get('/admin/lecturer/researche/:id', auth.isAuthenticated(), LecturerResearcheController.index);
router.get('/admin/lecturer/researche/get/all/:lectureId', auth.isAuthenticated(), LecturerResearcheController.get);
router.get('/admin/lecturer/researche/get/all', auth.isAuthenticated(), LecturerResearcheController.getAll);
router.post('/admin/lecturer/researche/p/add', auth.isAuthenticated(), LecturerResearcheController.add);
router.post('/admin/lecturer/researche/p/remove', auth.isAuthenticated(), LecturerResearcheController.remove);
router.get('/admin/lecturer/researche/get/:id', auth.isAuthenticated(),  LecturerResearcheController.dataSingle);
router.post('/admin/lecturer/researche/p/update/:id', auth.isAuthenticated(), LecturerResearcheController.update);

// news
router.get('/admin/news', auth.isAuthenticated(), NewsController.index);
router.get('/admin/news/add', auth.isAuthenticated(), NewsController.addNews);
router.get('/admin/news/get', auth.isAuthenticated(), NewsController.get);
router.post('/admin/news/p/add', auth.isAuthenticated(), NewsController.add);
router.post('/admin/news/p/remove', auth.isAuthenticated(), NewsController.remove);
router.get('/admin/update/news/:id', NewsController.dataSingle);
router.post('/admin/news/p/update/:id', auth.isAuthenticated(), NewsController.update);

// Event
router.get('/admin/event', auth.isAuthenticated(), EventController.index);
router.get('/admin/event/get', auth.isAuthenticated(), EventController.get);
router.post('/admin/event/p/add', auth.isAuthenticated(), EventController.add);
router.post('/admin/event/p/remove', auth.isAuthenticated(), EventController.remove);
router.get('/admin/event/get/:id', auth.isAuthenticated(),  EventController.dataSingle);
router.post('/admin/event/p/update/:id', auth.isAuthenticated(), EventController.update);

// Event Records
router.get('/admin/eventrecord', auth.isAuthenticated(), EventRecordController.index);
router.get('/admin/eventrecord/get', auth.isAuthenticated(), EventRecordController.get);
router.post('/admin/eventrecord/p/add', auth.isAuthenticated(), EventRecordController.add);
router.post('/admin/eventrecord/p/remove', auth.isAuthenticated(), EventRecordController.remove);
router.get('/admin/eventrecord/get/:id', auth.isAuthenticated(),  EventRecordController.dataSingle);
router.post('/admin/eventrecord/p/update/:id', auth.isAuthenticated(), EventRecordController.update);

// Event Gallery
router.get('/admin/event/gallery/:id', auth.isAuthenticated(), EventRecordGalleryController.index);
router.get('/admin/event/gallery/get', auth.isAuthenticated(), EventRecordGalleryController.get);
router.post('/admin/event/gallery/p/add', auth.isAuthenticated(), EventRecordGalleryController.add);
router.post('/admin/event/gallery/p/remove/:id', auth.isAuthenticated(), EventRecordGalleryController.remove);
router.get('/admin/event/gallery/get/:id', auth.isAuthenticated(),  EventRecordGalleryController.dataSingle);
router.post('/admin/event/gallery/p/update/:id', auth.isAuthenticated(), EventRecordGalleryController.update);

// Labs
router.get('/admin/labs', auth.isAuthenticated(), LabsController.index);
router.get('/admin/labs/get', auth.isAuthenticated(), LabsController.get);
router.post('/admin/labs/p/add', auth.isAuthenticated(), LabsController.add);
router.post('/admin/labs/p/remove', auth.isAuthenticated(), LabsController.remove);
router.get('/admin/labs/get/:id', auth.isAuthenticated(),  LabsController.dataSingle);
router.post('/admin/labs/p/update/:id', auth.isAuthenticated(), LabsController.update);

// Event Gallery
router.get('/admin/labs/gallery/:id', auth.isAuthenticated(), LabGalleryController.index);
router.get('/admin/labs/gallery/get', auth.isAuthenticated(), LabGalleryController.get);
router.post('/admin/labs/gallery/p/add', auth.isAuthenticated(), LabGalleryController.add);
router.post('/admin/labs/gallery/p/remove/:id', auth.isAuthenticated(), LabGalleryController.remove);
router.get('/admin/labs/gallery/get/:id', auth.isAuthenticated(),  LabGalleryController.dataSingle);
router.post('/admin/labs/gallery/p/update/:id', auth.isAuthenticated(), LabGalleryController.update);

// Course
router.get('/admin/courses', auth.isAuthenticated(), CourseController.index);
router.get('/admin/courses/get', auth.isAuthenticated(), CourseController.get);
router.post('/admin/courses/p/add', auth.isAuthenticated(), CourseController.add);
router.post('/admin/courses/p/remove', auth.isAuthenticated(), CourseController.remove);
router.get('/admin/courses/get/:id', auth.isAuthenticated(),  CourseController.dataSingle);
router.post('/admin/courses/p/update/:id', auth.isAuthenticated(), CourseController.update);

// Course detail
router.get('/admin/courses/detail/:id', auth.isAuthenticated(), CourseDetailController.index);
router.get('/admin/courses/detail/get/:id', auth.isAuthenticated(), CourseDetailController.get);
router.post('/admin/courses/detail/p/add', auth.isAuthenticated(), CourseDetailController.add);
router.post('/admin/courses/detail/p/remove', auth.isAuthenticated(), CourseDetailController.remove);
router.get('/admin/courses/detail/single/get/:id', auth.isAuthenticated(),  CourseDetailController.dataSingle);
router.post('/admin/courses/detail/p/update/:id', auth.isAuthenticated(), CourseDetailController.update);

// Profile
router.get('/admin/profile', auth.isAuthenticated(), ProfileController.index);
router.get('/admin/profile/add', auth.isAuthenticated(), ProfileController.addProfile);
router.post('/admin/profile/p/add', auth.isAuthenticated(), ProfileController.add);
router.post('/admin/profile/p/update/:id', auth.isAuthenticated(), ProfileController.update);

// Home
router.get('/admin/home', auth.isAuthenticated(), HomeController.index);
router.get('/admin/home/add', auth.isAuthenticated(), HomeController.addHome);
router.post('/admin/home/p/add', auth.isAuthenticated(), HomeController.add);
router.get('/admin/home/get/:id', auth.isAuthenticated(),  HomeController.dataSingle);
router.post('/admin/home/p/remove/:id', auth.isAuthenticated(), HomeController.remove);
router.post('/admin/home/p/update/:id', auth.isAuthenticated(), HomeController.update);

// Home
router.get('/admin/organization', auth.isAuthenticated(), OrganizationController.index);
router.get('/admin/organization/add', auth.isAuthenticated(), OrganizationController.addOrganization);
router.post('/admin/organization/p/add', auth.isAuthenticated(), OrganizationController.add);
router.post('/admin/organization/p/update/:id', auth.isAuthenticated(), OrganizationController.update);

// instructures
router.get('/admin/instructures', auth.isAuthenticated(), InstructureController.index);
router.get('/admin/instructures/get', auth.isAuthenticated(), InstructureController.get);
router.post('/admin/instructures/p/add', auth.isAuthenticated(), InstructureController.add);
router.post('/admin/instructures/p/remove', auth.isAuthenticated(), InstructureController.remove);
router.get('/admin/instructures/get/:id', auth.isAuthenticated(),  InstructureController.dataSingle);
router.post('/admin/instructures/p/update/:id', auth.isAuthenticated(), InstructureController.update);

// affiliate
router.get('/admin/affiliate', auth.isAuthenticated(), AffiliateController.index);
router.get('/admin/affiliate/get', auth.isAuthenticated(), AffiliateController.get);
router.post('/admin/affiliate/p/add', auth.isAuthenticated(), AffiliateController.add);
router.post('/admin/affiliate/p/remove', auth.isAuthenticated(), AffiliateController.remove);
router.get('/admin/affiliate/get/:id', auth.isAuthenticated(),  AffiliateController.dataSingle);
router.post('/admin/affiliate/p/update/:id', auth.isAuthenticated(), AffiliateController.update);

// academic year
router.get('/admin/academicYear', auth.isAuthenticated(), AcademicYearController.index);
router.get('/admin/academicYear/get', auth.isAuthenticated(), AcademicYearController.get);
router.post('/admin/academicYear/p/add', auth.isAuthenticated(), AcademicYearController.add);
router.post('/admin/academicYear/p/remove', auth.isAuthenticated(), AcademicYearController.remove);
router.get('/admin/academicYear/get/:id', auth.isAuthenticated(),  AcademicYearController.dataSingle);
router.post('/admin/academicYear/p/update/:id', auth.isAuthenticated(), AcademicYearController.update);

// academic calendar
router.get('/admin/calendar/:id', auth.isAuthenticated(), AcademicCalendarController.index);
router.get('/admin/calendar/get/all/:id', auth.isAuthenticated(), AcademicCalendarController.get);
router.post('/admin/calendar/p/add', auth.isAuthenticated(), AcademicCalendarController.add);
router.post('/admin/calendar/p/remove', auth.isAuthenticated(), AcademicCalendarController.remove);
router.get('/admin/calendar/get/:id', auth.isAuthenticated(),  AcademicCalendarController.dataSingle);
router.post('/admin/calendar/p/update/:id', auth.isAuthenticated(), AcademicCalendarController.update);

//-------------------END ETALASE DESA----------------------------------//


//--------------------FrontEnd------------------------------------------//
// Beranda
router.get('/', FrontHomeController.index);
router.get('/slider', FrontHomeController.slider);
// News
router.get('/news', FrontNewsController.index);
router.get('/news/:id', FrontNewsController.singleData);
// events
router.get('/events', FrontEventController.index);
router.get('/event/get/:id', FrontEventController.singleData);
// event record
router.get('/event/records', FrontEventRecordController.index);
router.get('/event/record/gallery/:id', FrontEventRecordController.singleData);

// profile
router.get('/organization', FrontOrganizationController.index);
router.get('/visionmission', FrontOrganizationController.vm);
router.get('/structure', FrontOrganizationController.structure);
router.get('/achievement', FrontOrganizationController.achievement);
router.get('/affiliates', FrontOrganizationController.affiliates);

// laboratorium
router.get('/labs', FrontLabController.index);
router.get('/lab/galleries/:id', FrontLabController.singleData);

// lecturer
router.get('/lecturers', FrontLecturerController.index);
router.get('/lecturer/detail/:id', FrontLecturerController.singleData);

// instructure
router.get('/instructure', FrontInstructureController.index);
router.get('/instructure/detail/:id', FrontInstructureController.singleData);

// instructure
router.get('/courses', FrontCourseController.index);
router.get('/courses/detail/:id', FrontCourseController.singleData);
router.get('/courses/detail/get/:id', FrontCourseController.lcmData);

router.get('/year', FrontCalendarController.index);
router.get('/year/detail/:id', FrontCalendarController.singleData);

// contact
router.get('/contact', FrontContactController.index);
// -----------------End FrontEnd------------------------------------------//
module.exports = router;
