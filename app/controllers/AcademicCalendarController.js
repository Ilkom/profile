'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);

module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;
      var user = req.user;
      model.user = user;
      model.yearId = req.params.id;

      res.render('content/academicCalendar', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(AcademicCalendar, 'datatables').forge()
        .paginate({
            request: req,
        })
        .query(function(qb) {
            qb.where('acc_year_id', '=', req.params.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    dataSingle: Async.route(function *(req, res, next) {
      var academicCalendar =  yield AcademicCalendar.where({id: req.params.id}).fetch().catch(console.error);
      res.ok(academicCalendar.toJSON());
   }),

    add: Async.route(function *(req, res, next) {

        const academicCalendar = AcademicCalendar.forge();
        const added = yield academicCalendar.save(req.body).catch(console.error);
        if (_.isEmpty(added)) {
            return res.ok('Failed to add academicCalendar');
        }
        return res.ok('1');
    }),

    remove: Async.route(function *(req, res, next) {
        // console.log(req.body);
        if (req.body.academicCalendarId.constructor === Array) {
            _.forEach(req.body.academicCalendarId, wrap(function *(v, i) {

              var academicCalendar = yield AcademicCalendar.forge({id: v}).fetch().catch(console.error);
              var academicCalendarData = academicCalendar.toJSON();

              const deleted = yield academicCalendar.destroy().catch(console.error);
                if (!_.isEmpty(deleted)) {
                    return res.ok('delete academicCalendar failed');
                }
            }));
            return res.ok('1');
        }else {
            var academicCalendar = yield AcademicCalendar.forge({id: req.body.academicCalendarId}).fetch().catch(console.error);
            var academicCalendarData = academicCalendar.toJSON();
            const deleted = yield academicCalendar.destroy().catch(console.error);
            if (_.isEmpty(deleted)) {
              return res.ok('delete academicCalendar failed');
            }
            return res.ok('1');
        }
    }),

    update: Async.route(function *(req, res, next) {

       var academicCalendars = yield AcademicCalendar.forge({id: req.params.id}).fetch().catch(console.error);
       const updated = yield academicCalendars.save(req.body).catch(console.error);
       if (_.isEmpty(updated)) {
           return res.ok('Failed to update AcademicCalendar');
       }

       return res.ok('1');
   }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
