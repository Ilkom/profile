'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;
      var user = req.user;
      model.user = user;
      
      res.render('content/courses', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(Course, 'datatables').forge()
        .paginate({
            request: req,
        })
        .query(function(qb) {
            // qb.where('id', '!=', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    dataSingle: Async.route(function *(req, res, next) {
      var courses =  yield Course.where({id: req.params.id}).fetch().catch(console.error);
      res.ok(courses.toJSON());
   }),

    add: Async.route(function *(req, res, next) {
      console.log(req.body);
        const courses = Course.forge();
        const added = yield courses.save(req.body).catch(console.error);
        if (_.isEmpty(added)) {
            return res.ok('Failed to add courses');
        }
        return res.ok('1');
    }),

    remove: Async.route(function *(req, res, next) {
        // console.log(req.body);
        if (req.body.coursesId.constructor === Array) {
            _.forEach(req.body.coursesId, wrap(function *(v, i) {

              var courses = yield Course.forge({id: v}).fetch().catch(console.error);
              var coursesData = courses.toJSON();

              const deleted = yield courses.destroy().catch(console.error);
                if (!_.isEmpty(deleted)) {
                    return res.ok('delete courses failed');
                }
            }));
            return res.ok('1');
        }else {
            var courses = yield Course.forge({id: req.body.coursesId}).fetch().catch(console.error);
            var coursesData = courses.toJSON();
            const deleted = yield courses.destroy().catch(console.error);
            if (_.isEmpty(deleted)) {
              return res.ok('delete courses failed');
            }
            return res.ok('1');
        }
    }),

    update: Async.route(function *(req, res, next) {
       var coursess = yield Course.forge({id: req.params.id}).fetch().catch(console.error);
       const updated = yield coursess.save(req.body).catch(console.error);
       if (_.isEmpty(updated)) {
           return res.ok('Failed to update Beritas');
       }
       return res.ok('1');
   }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
