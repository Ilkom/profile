'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

const pathUpload = 'assets/uploads/affiliate';
const pathDb = '/uploads/affiliate/';

module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;
      var user = req.user;
      model.user = user;

      res.render('content/affiliate', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(Affiliate, 'datatables').forge()
        .paginate({
            request: req,
        })
        .query(function(qb) {
            // qb.where('id', '!=', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    dataSingle: Async.route(function *(req, res, next) {
      var affiliate =  yield Affiliate.where({id: req.params.id}).fetch().catch(console.error);
      res.ok(affiliate.toJSON());
   }),

    add: Async.route(function *(req, res, next) {
        if (!_.isEmpty(req.body.srcDataCrop)) {
            const beritaImagePath = yield upload.base64('affiliate', pathUpload, req.body.srcDataCrop);

            if (!beritaImagePath) throw error('Gagal Menyimpan gambar', 500);
            req.body.aff_logo = path.join(pathDb,beritaImagePath);
        }

        delete req.body.srcDataCrop;

        const affiliate = Affiliate.forge();
        const added = yield affiliate.save(req.body).catch(console.error);
        if (_.isEmpty(added)) {
            return res.ok('Failed to add affiliate');
        }
        return res.ok('1');
    }),

    remove: Async.route(function *(req, res, next) {
        // console.log(req.body);
        if (req.body.affiliateId.constructor === Array) {
            _.forEach(req.body.affiliateId, wrap(function *(v, i) {

              var affiliate = yield Affiliate.forge({id: v}).fetch().catch(console.error);
              var affiliateData = affiliate.toJSON();

              const deleted = yield affiliate.destroy().catch(console.error);
                if (!_.isEmpty(deleted)) {
                    return res.ok('delete affiliate failed');
                }
            }));
            return res.ok('1');
        }else {
            var affiliate = yield Affiliate.forge({id: req.body.affiliateId}).fetch().catch(console.error);
            var affiliateData = affiliate.toJSON();
            const deleted = yield affiliate.destroy().catch(console.error);
            if (_.isEmpty(deleted)) {
              return res.ok('delete affiliate failed');
            }
            return res.ok('1');
        }
    }),

    update: Async.route(function *(req, res, next) {

      if (!_.isEmpty(req.body.srcDataCrop)) {
          const beritaImagePath = yield upload.base64('affiliate', pathUpload, req.body.srcDataCrop);

          if (!beritaImagePath) throw error('Gagal Menyimpan gambar', 500);
          req.body.aff_logo = path.join(pathDb,beritaImagePath);
      }

      delete req.body.srcDataCrop;

       var affiliates = yield Affiliate.forge({id: req.params.id}).fetch().catch(console.error);
       const updated = yield affiliates.save(req.body).catch(console.error);
       if (_.isEmpty(updated)) {
           return res.ok('Failed to update Affiliate');
       }

       return res.ok('1');
   }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
