'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');


module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;

      var profile =  yield Organization.where({id: 1}).fetch().catch(console.error);
      model.contact = profile.toJSON();

      // labs
      var labs =  yield Labs.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      model.labs = labs.toJSON();

      // lecturer
      req.query.limit = 8;
      var paginate = yield pm(Lecturer).forge()
      .limit(8)
      .paginate({
          request: req,
      }).query(function(qb) {
          qb.orderBy('id', 'Desc');
      })
      .end();
      model.paginate = paginate;
      if (req.query.page) {
          model.paginate.page = req.query.page;
      }else{
          model.paginate.page = 1;
      }

      // res.ok(model);
      res.render('front/lecturers', model);
    }),

    singleData:Async.route(function *(req, res, next) {
      const model = res.model;

      // labs
      var labs =  yield Labs.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      model.labs = labs.toJSON();

      var profile =  yield Organization.where({id: 1}).fetch().catch(console.error);
      model.contact = profile.toJSON();

      var lecturer =  yield Lecturer.where({id: req.params.id}).fetch().catch(console.error);
      model.lecturer = lecturer.toJSON();

      var educations =  yield LecturerEducations.query({ where: { ledu_lecturer_id: req.params.id }}).fetch().catch(console.error);
      model.educations = educations.toJSON();

      var achievement =  yield LecturerAchievements.query({ where: { lach_lecturer_id: req.params.id }}).fetch().catch(console.error);
      model.achievement = achievement.toJSON();

      var researche =  yield LecturerResearches.query({ where: { lres_lecturer_id: req.params.id }}).fetch().catch(console.error);
      model.researche = researche.toJSON();

      var expertises =  yield LecturerExpertises.query({ where: { le_lecturer_id: req.params.id }}).fetch().catch(console.error);
      model.expertises = expertises.toJSON();

      // console.log(model);
      res.render('front/lecturer_detail', model);
    }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
