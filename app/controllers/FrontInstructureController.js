'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');


module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;

      // labs
      var labs =  yield Labs.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      model.labs = labs.toJSON();

      var profile =  yield Organization.where({id: 1}).fetch().catch(console.error);
      model.contact = profile.toJSON();

      var instructures =  yield Instructures.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      model.instructures = instructures.toJSON();

      // console.log(model);
      res.render('front/instructures', model);
    }),

    singleData:Async.route(function *(req, res, next) {
      const model = res.model;

      // labs
      var labs =  yield Labs.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      model.labs = labs.toJSON();
      
      var profile =  yield Organization.where({id: 1}).fetch().catch(console.error);
      model.contact = profile.toJSON();

      var instructure =  yield Instructure.where({id: req.params.id}).fetch().catch(console.error);
      model.instructure = instructure.toJSON();

      // console.log(model);
      res.render('front/instructure_detail', model);
    }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
