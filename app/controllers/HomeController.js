'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

const pathUpload = 'assets/uploads/home';
const pathDb = '/uploads/home/';

module.exports = {

    index: Async.route(function *(req, res, next) {

      const model = res.model;
      var user = req.user;
      model.user = user;
      
      const homes =  yield Homes.forge().fetch().catch(console.error);
      if (homes) {
          model.homes =  homes.toJSON();
      }else{
          model.homes =0;
      }
      // res.ok(model);
      res.render('content/home', model);
    }),

    addHome: Async.route(function *(req, res, next) {
      const model = res.model;
      const homes =  yield Home.where({id: 1}).fetch().catch(console.error);
      if (homes) {
        res.redirect('/admin/home');
      }else{
        res.render('content/home_add', model);
      }

    }),

    add: Async.route(function *(req, res, next) {
        if (!_.isEmpty(req.body.srcDataCrop)) {
            const structureImagePath = yield upload.base64('structure', pathUpload, req.body.srcDataCrop);

            if (!structureImagePath) throw error('Gagal Menyimpan Latar', 500);
            req.body.hom_background_img = path.join(pathDb,structureImagePath);
        }
        delete req.body.srcDataCrop;
        const home = Home.forge();
        const added = yield home.save(req.body).catch(console.error);
        if (_.isEmpty(added)) {
            return res.ok('Failed to add home');
        }
        return res.ok('1');
    }),

    dataSingle: Async.route(function *(req, res, next) {
      var home =  yield Home.where({id: req.params.id}).fetch().catch(console.error);
      res.ok(home.toJSON());
   }),

    remove: Async.route(function *(req, res, next) {
      var home = yield Home.forge({id: req.params.id}).fetch().catch(console.error);
      var homeData = home.toJSON();
      const deleted = yield home.destroy().catch(console.error);
      if (_.isEmpty(deleted)) {
        return res.ok('delete home failed');
      }
      return res.ok('1');
    }),

    update: Async.route(function *(req, res, next) {

      if (!_.isEmpty(req.body.srcDataCrop)) {
          const homeImagePath = yield upload.base64('structure', pathUpload, req.body.srcDataCrop);

          if (!homeImagePath) throw error('Gagal Menyimpan Latar', 500);
          req.body.hom_background_img = path.join(pathDb,homeImagePath);
      }

      delete req.body.srcDataCrop;

       var homes = yield Home.forge({id: req.params.id}).fetch().catch(console.error);
       const updated = yield homes.save(req.body).catch(console.error);
       if (_.isEmpty(updated)) {
           return res.ok('Failed to update home');
       }

       return res.ok('1');
   }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
