'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;
      var user = req.user;
      model.user = user;
      
      var courses =  yield Course.where({id: req.params.id}).fetch().catch(console.error);
      model.course = courses.toJSON();

      var lecturers =  yield Lecturers.forge().orderBy('lec_name', 'Asc').fetch().catch(console.error);
      model.lecturers = lecturers.toJSON();

      res.render('content/course_detail', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(LecturerCourse, 'datatables').forge()
        .paginate({
            request: req,
            withRelated: ['lecturer','courses','lcmaterial',],
        })
        .query(function(qb) {
            qb.where('lc_course_id', '=', req.params.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    dataSingle: Async.route(function *(req, res, next) {
      var courses =  yield LecturerCourse.where({id: req.params.id}).fetch({withRelated:['lecturer','courses','lcmaterial']}).catch(console.error);
      res.ok(courses.toJSON());
   }),

    add: Async.route(function *(req, res, next) {

      var lecatureCourse = {
          'lc_course_id' : req.body.lc_course_id,
          'lc_lecturer_id' : req.body.lc_lecturer_id,
      }

      const courses = LecturerCourse.forge();
      const added = yield courses.save(lecatureCourse).catch(console.error);
      if (_.isEmpty(added)) {
          return res.ok('Failed to add courses');
      }

      var lcm = {
          'lcm_lc_id' : added.toJSON().id,
          'lcm_name' : req.body.lcm_name,
          'lcm_url' : req.body.lcm_url,
          'lcm_description' : req.body.lcm_description,
      }

      const lcmaterial = LcMaterial.forge();
      const added2 = yield lcmaterial.save(lcm).catch(console.error);
      if (_.isEmpty(added2)) {
          return res.ok('Failed to add Lecturer Courses Materials');
      }

      return res.ok('1');
    }),

    remove: Async.route(function *(req, res, next) {
        // console.log(req.body);
        if (req.body.coursesId.constructor === Array) {
            _.forEach(req.body.coursesId, wrap(function *(v, i) {

              var lcm = yield LcMaterial.forge({lcm_lc_id: v}).fetch().catch(console.error);
              var lcmData = lcm.toJSON();
              const deleted2 = yield lcm.destroy().catch(console.error);

              var lecturerCourse = yield LecturerCourse.forge({id: v}).fetch().catch(console.error);
              var lecturerCourseData = lecturerCourse.toJSON();

              const deleted = yield lecturerCourse.destroy().catch(console.error);
                if (!_.isEmpty(deleted)) {
                    return res.ok('delete lecturerCourse failed');
                }
            }));
            return res.ok('1');
        }else {
            var lcm = yield LcMaterial.forge({lcm_lc_id: req.body.coursesId}).fetch().catch(console.error);
            var lcmData = lcm.toJSON();
            const deleted2 = yield lcm.destroy().catch(console.error);

            var lecturerCourses = yield LecturerCourse.forge({id: req.body.coursesId}).fetch().catch(console.error);
            var lecturerCoursesData = lecturerCourses.toJSON();
            const deleted = yield lecturerCourses.destroy().catch(console.error);
            if (_.isEmpty(deleted)) {
              return res.ok('delete courses failed');
            }
            return res.ok('1');
        }
    }),

    update: Async.route(function *(req, res, next) {

      var lecatureCourse = {
          'lc_lecturer_id' : req.body.lc_lecturer_id,
      }

       var lc = yield LecturerCourse.forge({id: req.params.id}).fetch().catch(console.error);
       const updated = yield lc.save(lecatureCourse).catch(console.error);
       if (_.isEmpty(updated)) {
           return res.ok('Failed to update Lecturer Course');
       }

       var lcm = {
           'lcm_name' : req.body.lcm_name,
           'lcm_url' : req.body.lcm_url,
           'lcm_description' : req.body.lcm_description,
       }

       var lcmaterial = yield LcMaterial.forge({lcm_lc_id: req.params.id}).fetch().catch(console.error);
       const updated2 = yield lcmaterial.save(lcm).catch(console.error);
       if (_.isEmpty(updated2)) {
           return res.ok('Failed to update Lecturer Course Material');
       }

       return res.ok('1');
   }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
