'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

const pathUpload = 'assets/uploads/lecturer';
const pathDb = '/uploads/lecturer/';

module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;
      var user = req.user;
      model.user = user;
      
      res.render('content/lecturers', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(Lecturer, 'datatables').forge()
        .paginate({
            request: req,
        })
        .query(function(qb) {
            // qb.where('id', '!=', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    dataSingle: Async.route(function *(req, res, next) {
      var lecturers =  yield Lecturer.where({id: req.params.id}).fetch().catch(console.error);
      res.ok(lecturers.toJSON());
   }),

    add: Async.route(function *(req, res, next) {
      // if (req.body.acc_password == req.body.acc_repass) {
        if (!_.isEmpty(req.body.srcDataCrop)) {
            const lecturersImagePath = yield upload.base64('lecturers', pathUpload, req.body.srcDataCrop);
            if (!lecturersImagePath) throw error('Gagal Menyimpan gambar', 500);
            req.body.lec_photo = path.join(pathDb,lecturersImagePath);
        }

        delete req.body.srcDataCrop;

        // if your add passHash
        // delete req.body.repass;
        // var passHash = bcrypt.hashSync(req.body.acc_password, 8);
        // req.body.acc_password = passHash;
        // req.body.lec_email = req.body.acc_email;

        var lecturers = {
            'lec_nip' : req.body.lec_nip,
            'lec_name' : req.body.lec_name,
            'lec_nickname' : req.body.lec_nickname,
            'lec_photo' : req.body.lec_photo,
            'lec_gender' : req.body.lec_gender,
            'lec_birthplace' : req.body.lec_birthplace,
            'lec_birthdate' : req.body.lec_birthdate,
            'lec_handphone_number' : req.body.lec_handphone_number,
            'lec_email' : req.body.lec_email,
            'lec_photo' : req.body.lec_photo,
            'lec_address' : req.body.lec_address,
            'lec_city' : req.body.lec_city
        }

        const lecturer = Lecturer.forge();
        const added = yield lecturer.save(lecturers).catch(console.error);
        if (_.isEmpty(added)) {
            return res.ok('Failed to add lecturers');
        }

        // var account = {
        //   'id' : added.toJSON().id,
        //   'acc_email' : added.toJSON().lec_email,
        //   'acc_password' : req.body.acc_password,
        //   'acc_user_right' : req.body.acc_user_right,
        // }
        //
        // const accounts = Account.forge();
        // const added2 = yield accounts.save(account).catch(console.error);
        // if (_.isEmpty(added2)) {
        //     return res.ok('Failed to add account');
        // }

        return res.ok('1');

      // }else{
      //   return res.ok('Konfirmasi password tidak sesuai');
      // }

    }),

    remove: Async.route(function *(req, res, next) {
        // console.log(req.body);
        if (req.body.lecturersId.constructor === Array) {
            _.forEach(req.body.lecturersId, wrap(function *(v, i) {

              var lecturers = yield Lecturer.forge({id: v}).fetch().catch(console.error);
              var lecturersData = lecturers.toJSON();

              const deleted = yield lecturers.destroy().catch(console.error);
                if (!_.isEmpty(deleted)) {
                    return res.ok('delete lecturers failed');
                }
            }));
            return res.ok('1');
        }else {
            var lecturers = yield Lecturer.forge({id: req.body.lecturersId}).fetch().catch(console.error);
            var lecturersData = lecturers.toJSON();
            const deleted = yield lecturers.destroy().catch(console.error);
            if (_.isEmpty(deleted)) {
              return res.ok('delete lecturers failed');
            }
            return res.ok('1');
        }
    }),

    update: Async.route(function *(req, res, next) {
      if (!_.isEmpty(req.body.srcDataCrop)) {
          const lecturerImagePath = yield upload.base64('lecturer', pathUpload, req.body.srcDataCrop);

          if (!lecturerImagePath) throw error('Gagal Menyimpan gambar', 500);
          req.body.lec_photo= path.join(pathDb,lecturerImagePath);
      }

      delete req.body.srcDataCrop;

       var lecturer = yield Lecturer.forge({id: req.params.id}).fetch().catch(console.error);
       const updated = yield lecturer.save(req.body).catch(console.error);
       if (_.isEmpty(updated)) {
           return res.ok('Failed to update Administrasi');
       }

       return res.ok('1');
   }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
