'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

const pathUpload = 'assets/uploads/organization';
const pathDb = '/uploads/organization/';

module.exports = {

    index: Async.route(function *(req, res, next) {

      const model = res.model;
      var user = req.user;
      model.user = user;
      
      const organizations =  yield Organization.where({id: 1}).fetch().catch(console.error);
      if (organizations) {
          model.organizations =  organizations.toJSON();
      }else{
          model.organizations =0;
      }
      // res.ok(model);
      res.render('content/organization', model);
    }),

    addOrganization: Async.route(function *(req, res, next) {
      const model = res.model;
      const organizations =  yield Organization.where({id: 1}).fetch().catch(console.error);
      if (organizations) {
        res.redirect('/admin/organization');
      }else{
        res.render('content/organization_add', model);
      }

    }),

    add: Async.route(function *(req, res, next) {
        if (!_.isEmpty(req.body.srcDataCrop)) {
            const structureImagePath = yield upload.base64('structure', pathUpload, req.body.srcDataCrop);

            if (!structureImagePath) throw error('Gagal Menyimpan Logo', 500);
            req.body.org_logo = path.join(pathDb,structureImagePath);
        }
        delete req.body.srcDataCrop;
        const organization = Organization.forge();
        const added = yield organization.save(req.body).catch(console.error);
        if (_.isEmpty(added)) {
            return res.ok('Failed to add organization');
        }
        return res.ok('1');
    }),

    update: Async.route(function *(req, res, next) {

      if (!_.isEmpty(req.body.srcDataCrop)) {
          const organizationImagePath = yield upload.base64('structure', pathUpload, req.body.srcDataCrop);

          if (!organizationImagePath) throw error('Gagal Menyimpan Logo', 500);
          req.body.org_logo = path.join(pathDb,organizationImagePath);
      }

      delete req.body.srcDataCrop;

       var organizations = yield Organization.forge({id: req.params.id}).fetch().catch(console.error);
       const updated = yield organizations.save(req.body).catch(console.error);
       if (_.isEmpty(updated)) {
           return res.ok('Failed to update organization');
       }

       return res.ok('1');
   }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
