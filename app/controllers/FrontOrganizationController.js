'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');


module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;

      // labs
      var labs =  yield Labs.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      model.labs = labs.toJSON();

      var profile =  yield Organization.where({id: 1}).fetch().catch(console.error);
      model.contact = profile.toJSON();

      const profiles =  yield Profile.where({id: 1}).fetch().catch(console.error);
      if (profiles) {
          model.profile =  profiles.toJSON();
      }else{
          model.profile =0;
      }
      // console.log(model);
      res.render('front/organization', model);
    }),

    vm: Async.route(function *(req, res, next) {
      const model = res.model;

      // labs
      var labs =  yield Labs.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      model.labs = labs.toJSON();

      var profile =  yield Organization.where({id: 1}).fetch().catch(console.error);
      model.contact = profile.toJSON();

      const profiles =  yield Profile.where({id: 1}).fetch().catch(console.error);
      if (profiles) {
          model.profile =  profiles.toJSON();
      }else{
          model.profile =0;
      }
      // console.log(model);
      res.render('front/visionmission', model);
    }),

    structure: Async.route(function *(req, res, next) {
      const model = res.model;

      // labs
      var labs =  yield Labs.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      model.labs = labs.toJSON();

      var profile =  yield Organization.where({id: 1}).fetch().catch(console.error);
      model.contact = profile.toJSON();

      const profiles =  yield Profile.where({id: 1}).fetch().catch(console.error);
      if (profiles) {
          model.profile =  profiles.toJSON();
      }else{
          model.profile =0;
      }
      // console.log(model);
      res.render('front/structure', model);
    }),

    achievement: Async.route(function *(req, res, next) {
      const model = res.model;

      // labs
      var labs =  yield Labs.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      model.labs = labs.toJSON();

      var profile =  yield Organization.where({id: 1}).fetch().catch(console.error);
      model.contact = profile.toJSON();


      var achievements =  yield LecturerAchievements.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      model.achievements = achievements.toJSON();

      // console.log(model);

      // res.ok(model);
      res.render('front/achievement', model);
    }),

    affiliates: Async.route(function *(req, res, next) {
      const model = res.model;

      // labs
      var labs =  yield Labs.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      model.labs = labs.toJSON();

      var profile =  yield Organization.where({id: 1}).fetch().catch(console.error);
      model.contact = profile.toJSON();

      req.query.limit = 8;
      var paginate = yield pm(Affiliate).forge()
      .limit(8)
      .paginate({
          request: req,
      }).query(function(qb) {
          qb.orderBy('id', 'Desc');
      })
      .end();
      model.paginate = paginate;
      if (req.query.page) {
          model.paginate.page = req.query.page;
      }else{
          model.paginate.page = 1;
      }

      // console.log(model);
      // res.ok(model);
      res.render('front/affiliates', model);
    }),

    singleData:Async.route(function *(req, res, next) {
      var event =  yield Event.where({id: req.params.id}).fetch().catch(console.error);
      res.ok(event.toJSON());
    }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
