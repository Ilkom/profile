'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');


module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;

      // labs
      var labs =  yield Labs.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      model.labs = labs.toJSON();

      var profile =  yield Organization.where({id: 1}).fetch().catch(console.error);
      model.contact = profile.toJSON();

      // event
      req.query.limit = 8;
      var paginate = yield pm(Event).forge()
      .limit(8)
      .paginate({
          request: req,
      }).query(function(qb) {
          qb.orderBy('id', 'Desc');
      })
      .end();
      model.paginate = paginate;
      if (req.query.page) {
          model.paginate.page = req.query.page;
      }else{
          model.paginate.page = 1;
      }

      // var event =  yield Events.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      // model.events = event.toJSON();
      // console.log(model);
      // res.ok(model);

      res.render('front/events', model);
    }),

    singleData:Async.route(function *(req, res, next) {
      var event =  yield Event.where({id: req.params.id}).fetch().catch(console.error);
      res.ok(event.toJSON());
    }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
