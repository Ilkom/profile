'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

const pathUpload = 'assets/uploads/leader';
const pathDb = '/uploads/leader/';

module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;
      var user = req.user;
      model.user = user;

      res.render('content/leaders', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(Leader, 'datatables').forge()
        .paginate({
            request: req,
        })
        .query(function(qb) {
            // qb.where('id', '!=', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    dataSingle: Async.route(function *(req, res, next) {
      var leaders =  yield Leader.where({id: req.params.id}).fetch().catch(console.error);
      res.ok(leaders.toJSON());
   }),

    add: Async.route(function *(req, res, next) {
      // if (req.body.acc_password == req.body.acc_repass) {
        if (!_.isEmpty(req.body.srcDataCrop)) {
            const leadersImagePath = yield upload.base64('leaders', pathUpload, req.body.srcDataCrop);
            if (!leadersImagePath) throw error('Gagal Menyimpan gambar', 500);
            req.body.lea_photo = path.join(pathDb,leadersImagePath);
        }

        delete req.body.srcDataCrop;

        // if your add passHash
        // delete req.body.repass;

        // req.body.acc_password = passHash;
        // req.body.lea_email = req.body.acc_email;

        var leaders = {
            'lea_nip' : req.body.lea_nip,
            'lea_name' : req.body.lea_name,
            'lea_nickname' : req.body.lea_nickname,
            'lea_photo' : req.body.lea_photo,
            'lea_gender' : req.body.lea_gender,
            'lea_birthplace' : req.body.lea_birthplace,
            'lea_birthdate' : req.body.lea_birthdate,
            'lea_handphone_number' : req.body.lea_handphone_number,
            'lea_email' : req.body.lea_email,
            'lea_photo' : req.body.lea_photo,
            'lea_address' : req.body.lea_address,
            'lea_city' : req.body.lea_city
        }

        const leader = Leader.forge();
        const added = yield leader.save(leaders).catch(console.error);
        if (_.isEmpty(added)) {
            return res.ok('Failed to add leaders');
        }

        var passHash = bcrypt.hashSync('ilkom123', 8);
        var account = {
          'acc_email' : added.toJSON().lea_email,
          'acc_password' : passHash,
          'acc_user_right' : 1,
        }

        const accounts = Account.forge();
        const added2 = yield accounts.save(account).catch(console.error);
        if (_.isEmpty(added2)) {
            return res.ok('Failed to add account');
        }

        return res.ok('1');

      // }else{
      //   return res.ok('Konfirmasi password tidak sesuai');
      // }

    }),

    remove: Async.route(function *(req, res, next) {
        // console.log(req.body);
        if (req.body.leadersId.constructor === Array) {
            _.forEach(req.body.leadersId, wrap(function *(v, i) {

              var leaders = yield Leader.forge({id: v}).fetch().catch(console.error);
              var leadersData = leaders.toJSON();

              const deleted = yield leaders.destroy().catch(console.error);
                if (!_.isEmpty(deleted)) {
                    return res.ok('delete leaders failed');
                }
            }));
            return res.ok('1');
        }else {
            var leaders = yield Leader.forge({id: req.body.leadersId}).fetch().catch(console.error);
            var leadersData = leaders.toJSON();
            const deleted = yield leaders.destroy().catch(console.error);
            if (_.isEmpty(deleted)) {
              return res.ok('delete leaders failed');
            }
            return res.ok('1');
        }
    }),

    update: Async.route(function *(req, res, next) {
      if (!_.isEmpty(req.body.srcDataCrop)) {
          const leaderImagePath = yield upload.base64('leader', pathUpload, req.body.srcDataCrop);

          if (!leaderImagePath) throw error('Gagal Menyimpan gambar', 500);
          req.body.lea_photo= path.join(pathDb,leaderImagePath);
      }

      delete req.body.srcDataCrop;

       var leader = yield Leader.forge({id: req.params.id}).fetch().catch(console.error);
       if (req.body.lea_email) {
         var email = {acc_email:req.body.lea_email};
           var account = yield Account.forge({acc_email: leader.toJSON().lea_email}).fetch().catch(console.error);
           const updatedAcc = yield account.save(email).catch(console.error);
           if (_.isEmpty(updatedAcc)) {
               return res.ok('Failed to update Account');
           }
       }

       const updated = yield leader.save(req.body).catch(console.error);
       if (_.isEmpty(updated)) {
           return res.ok('Failed to update Administrasi');
       }

       return res.ok('1');
   }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
