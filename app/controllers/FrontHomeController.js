'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');


module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;
      var profile =  yield Organization.where({id: 1}).fetch().catch(console.error);
      model.contact = profile.toJSON();

      // news
      var news =  yield Newses.forge().query(function (qb) {
          qb.limit(4);
      }).orderBy('id', 'Desc').fetch().catch(console.error);
      model.newses = news.toJSON();

      // event
      var events =  yield Events.forge().query(function (qb) {
          qb.limit(4);
      }).orderBy('id', 'Desc').fetch().catch(console.error);
      model.events = events.toJSON();

      const profiles =  yield Profile.where({id: 1}).fetch().catch(console.error);
      if (profiles) {
          model.profile =  profiles.toJSON();
      }else{
          model.profile =0;
      }
      
      // labs
      var labs =  yield Labs.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      model.labs = labs.toJSON();

      res.render('front/home', model);
    }),

    slider: Async.route(function *(req, res, next) {
      var slider =  yield Homes.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      res.ok(slider.toJSON());

    }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
