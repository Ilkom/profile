'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

const pathUpload = 'assets/uploads/gallery';
const pathDb = '/uploads/gallery/';

module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;
      var user = req.user;
      model.user = user;
      
      var labGallerys =  yield LabGalleries.query({ where: { lgal_lab_id: req.params.id }}).fetch().catch(console.error);
      model.galleries = labGallerys.toJSON();
      model.labRecordId = req.params.id;
      // res.ok(model);
      res.render('content/lab_gallery', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(LabGallery, 'datatables').forge()
        .paginate({
            request: req,
        })
        .query(function(qb) {
            // qb.where('id', '!=', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    dataSingle: Async.route(function *(req, res, next) {
      var labGallery =  yield LabGallery.where({id: req.params.id}).fetch().catch(console.error);
      res.ok(labGallery.toJSON());
   }),


    add: Async.route(function *(req, res, next) {
        if (!_.isEmpty(req.body.srcDataCrop)) {
            const galleryImagePath = yield upload.base64('gallery', pathUpload, req.body.srcDataCrop);

            if (!galleryImagePath) throw error('Gagal Menyimpan gambar', 500);
            req.body.lgal_photo = path.join(pathDb,galleryImagePath);
        }

        delete req.body.srcDataCrop;

        const labGallery = LabGallery.forge();
        const added = yield labGallery.save(req.body).catch(console.error);
        if (_.isEmpty(added)) {
            return res.ok('Failed to add labGallery');
        }
        return res.ok('1');
    }),

    remove: Async.route(function *(req, res, next) {
      var labGallery = yield LabGallery.forge({id: req.params.id}).fetch().catch(console.error);
      var labGalleryData = labGallery.toJSON();
      const deleted = yield labGallery.destroy().catch(console.error);
      if (_.isEmpty(deleted)) {
        return res.ok('delete labGallery failed');
      }
      return res.ok('1');

    }),

    update: Async.route(function *(req, res, next) {

      if (!_.isEmpty(req.body.srcDataCrop)) {
          const galleryImagePath = yield upload.base64('labGallery', pathUpload, req.body.srcDataCrop);

          if (!galleryImagePath) throw error('Gagal Menyimpan gambar', 500);
          req.body.lgal_photo = path.join(pathDb,galleryImagePath);
      }

      delete req.body.srcDataCrop;

       var labGallerys = yield LabGallery.forge({id: req.params.id}).fetch().catch(console.error);
       const updated = yield labGallerys.save(req.body).catch(console.error);
       if (_.isEmpty(updated)) {
           return res.ok('Failed to update gallery');
       }

       return res.ok('1');
   }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
