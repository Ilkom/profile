'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

const pathUpload = 'assets/uploads/lecturer';
const pathDb = '/uploads/lecturer/';

module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;
      var user = req.user;
      model.user = user;
      
      var lecturers =  yield Lecturer.where({id: req.params.id}).fetch().catch(console.error);
      model.lecturer = lecturers.toJSON();

      // res.ok(lecturer.toJSON());
      res.render('content/lecturer_achievements', model);
    }),

    achievement: Async.route(function *(req, res, next) {
      const model = res.model;
      res.render('content/achievements', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(LecturerAchievement, 'datatables').forge()
        .paginate({
            request: req,
        })
        .query(function(qb) {
            qb.where('lach_lecturer_id', '=', req.params.lectureId);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    getAll: Async.route(function *(req, res, next) {
        pm(LecturerAchievement, 'datatables').forge()
        .paginate({
            request: req,
        })
        .query(function(qb) {
            // qb.where('ledu_lecturer_id', '==', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    dataSingle: Async.route(function *(req, res, next) {
      var lecturers =  yield LecturerAchievement.where({id: req.params.id}).fetch().catch(console.error);
      res.ok(lecturers.toJSON());
   }),

    add: Async.route(function *(req, res, next) {

        const lecturer = LecturerAchievement.forge();
        const added = yield lecturer.save(req.body).catch(console.error);
        if (_.isEmpty(added)) {
            return res.ok('Failed to add education of lecturer');
        }

        return res.ok('1');

    }),

    remove: Async.route(function *(req, res, next) {
        // console.log(req.body);
        if (req.body.lecturersId.constructor === Array) {
            _.forEach(req.body.lecturersId, wrap(function *(v, i) {

              var lecturers = yield LecturerAchievement.forge({id: v}).fetch().catch(console.error);
              var lecturersData = lecturers.toJSON();

              const deleted = yield lecturers.destroy().catch(console.error);
                if (!_.isEmpty(deleted)) {
                    return res.ok('delete lecturers failed');
                }
            }));
            return res.ok('1');
        }else {
            var lecturers = yield LecturerAchievement.forge({id: req.body.lecturersId}).fetch().catch(console.error);
            var lecturersData = lecturers.toJSON();
            const deleted = yield lecturers.destroy().catch(console.error);
            if (_.isEmpty(deleted)) {
              return res.ok('delete lecturers failed');
            }
            return res.ok('1');
        }
    }),

    update: Async.route(function *(req, res, next) {
      if (!_.isEmpty(req.body.srcDataCrop)) {
          const lecturerImagePath = yield upload.base64('lecturer', pathUpload, req.body.srcDataCrop);

          if (!lecturerImagePath) throw error('Gagal Menyimpan gambar', 500);
          req.body.lec_photo= path.join(pathDb,lecturerImagePath);
      }

      delete req.body.srcDataCrop;

       var lecturer = yield LecturerAchievement.forge({id: req.params.id}).fetch().catch(console.error);
       const updated = yield lecturer.save(req.body).catch(console.error);
       if (_.isEmpty(updated)) {
           return res.ok('Failed to update Administrasi');
       }

       return res.ok('1');
   }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
