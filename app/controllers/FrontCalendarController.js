'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');


module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;

      // labs
      var labs =  yield Labs.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      model.labs = labs.toJSON();

      var profile =  yield Organization.where({id: 1}).fetch().catch(console.error);
      model.contact = profile.toJSON();

      var years =  yield AcademicYears.forge().orderBy('id', 'DESC').fetch().catch(console.error);
      model.years = years.toJSON();

      // console.log(model);
      res.render('front/academic_year', model);
    }),

    singleData: Async.route(function *(req, res, next) {
      const model = res.model;

      // labs
      var labs =  yield Labs.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      model.labs = labs.toJSON();

      var profile =  yield Organization.where({id: 1}).fetch().catch(console.error);
      model.contact = profile.toJSON();

      var year =  yield AcademicYear.where({id: req.params.id}).fetch().catch(console.error);
      model.year = year.toJSON();

      var calendars =  yield AcademicCalendars.query({ where: { acc_year_id: req.params.id }}).orderBy('acc_date', 'ASC').fetch().catch(console.error);
      model.calendars = calendars.toJSON();
      // console.log(model);
      // res.ok(model);
      res.render('front/academic_calendar', model);
    }),

    lcmData:Async.route(function *(req, res, next) {
      var lcm =  yield LcMaterial.where({lcm_lc_id: req.params.id}).fetch().catch(console.error);
      res.ok(lcm.toJSON());
    }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
