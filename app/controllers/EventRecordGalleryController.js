'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

const pathUpload = 'assets/uploads/gallery';
const pathDb = '/uploads/gallery/';

module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;
      var user = req.user;
      model.user = user;

      var events =  yield EventRecordGalleries.query({ where: { ergal_event_record_id: req.params.id }}).fetch().catch(console.error);
      model.galleries = events.toJSON();
      model.eventRecordId = req.params.id;
      res.render('content/event_gallery', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(EventRecordGallery, 'datatables').forge()
        .paginate({
            request: req,
        })
        .query(function(qb) {
            // qb.where('id', '!=', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    dataSingle: Async.route(function *(req, res, next) {
      var event =  yield EventRecordGallery.where({id: req.params.id}).fetch().catch(console.error);
      res.ok(event.toJSON());
   }),


    add: Async.route(function *(req, res, next) {
        if (!_.isEmpty(req.body.srcDataCrop)) {
            const galleryImagePath = yield upload.base64('gallery', pathUpload, req.body.srcDataCrop);

            if (!galleryImagePath) throw error('Gagal Menyimpan gambar', 500);
            req.body.ergal_photo = path.join(pathDb,galleryImagePath);
        }

        delete req.body.srcDataCrop;

        const event = EventRecordGallery.forge();
        const added = yield event.save(req.body).catch(console.error);
        if (_.isEmpty(added)) {
            return res.ok('Failed to add event');
        }
        return res.ok('1');
    }),

    remove: Async.route(function *(req, res, next) {
      var event = yield EventRecordGallery.forge({id: req.params.id}).fetch().catch(console.error);
      var eventData = event.toJSON();
      const deleted = yield event.destroy().catch(console.error);
      if (_.isEmpty(deleted)) {
        return res.ok('delete event failed');
      }
      return res.ok('1');

    }),

    update: Async.route(function *(req, res, next) {

      if (!_.isEmpty(req.body.srcDataCrop)) {
          const beritaImagePath = yield upload.base64('event', pathUpload, req.body.srcDataCrop);

          if (!beritaImagePath) throw error('Gagal Menyimpan gambar', 500);
          req.body.ergal_photo = path.join(pathDb,beritaImagePath);
      }

      delete req.body.srcDataCrop;

       var events = yield EventRecordGallery.forge({id: req.params.id}).fetch().catch(console.error);
       const updated = yield events.save(req.body).catch(console.error);
       if (_.isEmpty(updated)) {
           return res.ok('Failed to update Beritas');
       }

       return res.ok('1');
   }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
