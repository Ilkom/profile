'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

const pathUpload = 'assets/uploads/profile';
const pathDb = '/uploads/profile/';

module.exports = {

    index: Async.route(function *(req, res, next) {

      const model = res.model;
      var user = req.user;
      model.user = user;
      
      const profiles =  yield Profile.where({id: 1}).fetch().catch(console.error);
      if (profiles) {
          model.profiles =  profiles.toJSON();
      }else{
          model.profiles =0;
      }
      // res.ok(model);
      res.render('content/profile', model);
    }),

    addProfile: Async.route(function *(req, res, next) {
      const model = res.model;
      const profiles =  yield Profile.where({id: 1}).fetch().catch(console.error);
      if (profiles) {
        res.redirect('/admin/profile');
      }else{
        res.render('content/profile_add', model);
      }

    }),

    add: Async.route(function *(req, res, next) {
        if (!_.isEmpty(req.body.srcDataCrop)) {
            const structureImagePath = yield upload.base64('structure', pathUpload, req.body.srcDataCrop);

            if (!structureImagePath) throw error('Gagal Menyimpan Struktur', 500);
            req.body.pro_structure_img = path.join(pathDb,structureImagePath);
        }
        delete req.body.srcDataCrop;
        const profile = Profile.forge();
        const added = yield profile.save(req.body).catch(console.error);
        if (_.isEmpty(added)) {
            return res.ok('Failed to add profile');
        }
        return res.ok('1');
    }),

    update: Async.route(function *(req, res, next) {

      if (!_.isEmpty(req.body.srcDataCrop)) {
          const profileImagePath = yield upload.base64('structure', pathUpload, req.body.srcDataCrop);

          if (!profileImagePath) throw error('Gagal Menyimpan Struktur', 500);
          req.body.pro_structure_img = path.join(pathDb,profileImagePath);
      }

      delete req.body.srcDataCrop;

       var profiles = yield Profile.forge({id: req.params.id}).fetch().catch(console.error);
       const updated = yield profiles.save(req.body).catch(console.error);
       if (_.isEmpty(updated)) {
           return res.ok('Failed to update profile');
       }

       return res.ok('1');
   }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
