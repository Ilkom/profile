'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

const pathUpload = 'assets/uploads/instructure';
const pathDb = '/uploads/instructure/';

module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;
      var user = req.user;
      model.user = user;
      
      res.render('content/instructures', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(Instructure, 'datatables').forge()
        .paginate({
            request: req,
        })
        .query(function(qb) {
            // qb.where('id', '!=', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    dataSingle: Async.route(function *(req, res, next) {
      var instructures =  yield Instructure.where({id: req.params.id}).fetch().catch(console.error);
      res.ok(instructures.toJSON());
   }),

    add: Async.route(function *(req, res, next) {
      // if (req.body.acc_password == req.body.acc_repass) {
        if (!_.isEmpty(req.body.srcDataCrop)) {
            const instructuresImagePath = yield upload.base64('instructures', pathUpload, req.body.srcDataCrop);
            if (!instructuresImagePath) throw error('Gagal Menyimpan gambar', 500);
            req.body.ins_photo = path.join(pathDb,instructuresImagePath);
        }

        delete req.body.srcDataCrop;

        // if your add passHash
        // delete req.body.repass;
        // var passHash = bcrypt.hashSync(req.body.acc_password, 8);
        // req.body.acc_password = passHash;
        // req.body.ins_email = req.body.acc_email;

        var instructures = {
            'ins_nip' : req.body.ins_nip,
            'ins_name' : req.body.ins_name,
            'ins_nickname' : req.body.ins_nickname,
            'ins_photo' : req.body.ins_photo,
            'ins_gender' : req.body.ins_gender,
            'ins_birthplace' : req.body.ins_birthplace,
            'ins_birthdate' : req.body.ins_birthdate,
            'ins_handphone_number' : req.body.ins_handphone_number,
            'ins_email' : req.body.ins_email,
            'ins_photo' : req.body.ins_photo,
            'ins_address' : req.body.ins_address,
            'ins_city' : req.body.ins_city
        }

        const instructure = Instructure.forge();
        const added = yield instructure.save(instructures).catch(console.error);
        if (_.isEmpty(added)) {
            return res.ok('Failed to add instructures');
        }

        return res.ok('1');

    }),

    remove: Async.route(function *(req, res, next) {
        // console.log(req.body);
        if (req.body.instructuresId.constructor === Array) {
            _.forEach(req.body.instructuresId, wrap(function *(v, i) {

              var instructures = yield Instructure.forge({id: v}).fetch().catch(console.error);
              var instructuresData = instructures.toJSON();

              const deleted = yield instructures.destroy().catch(console.error);
                if (!_.isEmpty(deleted)) {
                    return res.ok('delete instructures failed');
                }
            }));
            return res.ok('1');
        }else {
            var instructures = yield Instructure.forge({id: req.body.instructuresId}).fetch().catch(console.error);
            var instructuresData = instructures.toJSON();
            const deleted = yield instructures.destroy().catch(console.error);
            if (_.isEmpty(deleted)) {
              return res.ok('delete instructures failed');
            }
            return res.ok('1');
        }
    }),

    update: Async.route(function *(req, res, next) {
      if (!_.isEmpty(req.body.srcDataCrop)) {
          const InstructureImagePath = yield upload.base64('Instructure', pathUpload, req.body.srcDataCrop);

          if (!InstructureImagePath) throw error('Gagal Menyimpan gambar', 500);
          req.body.ins_photo= path.join(pathDb,InstructureImagePath);
      }

      delete req.body.srcDataCrop;

       var instructure = yield Instructure.forge({id: req.params.id}).fetch().catch(console.error);
       const updated = yield instructure.save(req.body).catch(console.error);
       if (_.isEmpty(updated)) {
           return res.ok('Failed to update Administrasi');
       }

       return res.ok('1');
   }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
