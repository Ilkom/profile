'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);

module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;
      var user = req.user;
      model.user = user;

      res.render('content/academicYear', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(AcademicYear, 'datatables').forge()
        .paginate({
            request: req,
        })
        .query(function(qb) {
            // qb.where('id', '!=', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    dataSingle: Async.route(function *(req, res, next) {
      var academicYear =  yield AcademicYear.where({id: req.params.id}).fetch().catch(console.error);
      res.ok(academicYear.toJSON());
   }),

    add: Async.route(function *(req, res, next) {

        const academicYear = AcademicYear.forge();
        const added = yield academicYear.save(req.body).catch(console.error);
        if (_.isEmpty(added)) {
            return res.ok('Failed to add academicYear');
        }
        return res.ok('1');
    }),

    remove: Async.route(function *(req, res, next) {
        // console.log(req.body);
        if (req.body.academicYearId.constructor === Array) {
            _.forEach(req.body.academicYearId, wrap(function *(v, i) {

              var academicYear = yield AcademicYear.forge({id: v}).fetch().catch(console.error);
              var academicYearData = academicYear.toJSON();

              const deleted = yield academicYear.destroy().catch(console.error);
                if (!_.isEmpty(deleted)) {
                    return res.ok('delete academicYear failed');
                }
            }));
            return res.ok('1');
        }else {
            var academicYear = yield AcademicYear.forge({id: req.body.academicYearId}).fetch().catch(console.error);
            var academicYearData = academicYear.toJSON();
            const deleted = yield academicYear.destroy().catch(console.error);
            if (_.isEmpty(deleted)) {
              return res.ok('delete academicYear failed');
            }
            return res.ok('1');
        }
    }),

    update: Async.route(function *(req, res, next) {
      
       var academicYears = yield AcademicYear.forge({id: req.params.id}).fetch().catch(console.error);
       const updated = yield academicYears.save(req.body).catch(console.error);
       if (_.isEmpty(updated)) {
           return res.ok('Failed to update AcademicYear');
       }

       return res.ok('1');
   }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
