'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;
      var user = req.user;
      model.user = user;
      
      res.render('content/labs', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(Lab, 'datatables').forge()
        .paginate({
            request: req,
        })
        .query(function(qb) {
            // qb.where('id', '!=', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    dataSingle: Async.route(function *(req, res, next) {
      var labs =  yield Lab.where({id: req.params.id}).fetch().catch(console.error);
      res.ok(labs.toJSON());
   }),

    add: Async.route(function *(req, res, next) {
        const labs = Lab.forge();
        const added = yield labs.save(req.body).catch(console.error);
        if (_.isEmpty(added)) {
            return res.ok('Failed to add labs');
        }
        return res.ok('1');
    }),

    remove: Async.route(function *(req, res, next) {
        // console.log(req.body);
        if (req.body.labId.constructor === Array) {
            _.forEach(req.body.labId, wrap(function *(v, i) {

              var labs = yield Lab.forge({id: v}).fetch().catch(console.error);
              var labsData = labs.toJSON();

              const deleted = yield labs.destroy().catch(console.error);
                if (!_.isEmpty(deleted)) {
                    return res.ok('delete labs failed');
                }
            }));
            return res.ok('1');
        }else {
            var labs = yield Lab.forge({id: req.body.labId}).fetch().catch(console.error);
            var labsData = labs.toJSON();
            const deleted = yield labs.destroy().catch(console.error);
            if (_.isEmpty(deleted)) {
              return res.ok('delete labs failed');
            }
            return res.ok('1');
        }
    }),

    update: Async.route(function *(req, res, next) {
       var labss = yield Lab.forge({id: req.params.id}).fetch().catch(console.error);
       const updated = yield labss.save(req.body).catch(console.error);
       if (_.isEmpty(updated)) {
           return res.ok('Failed to update Beritas');
       }
       return res.ok('1');
   }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
