'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');


module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;

      // labs
      var labs =  yield Labs.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      model.labs = labs.toJSON();

      var profile =  yield Organization.where({id: 1}).fetch().catch(console.error);
      model.contact = profile.toJSON();

      // var events =  yield EventRecords.forge().orderBy('id', 'Desc').fetch({withRelated: ['eventRecordGallery']}).catch(console.error);
      // model.events = events.toJSON();

      req.query.limit = 8;
      var paginate = yield pm(EventRecord).forge()
      .limit(8)
      .paginate({
          request: req,
          withRelated: ['eventRecordGallery'],
      }).query(function(qb) {
          qb.orderBy('id', 'Desc');
      })
      .end();
      model.paginate = paginate;
      if (req.query.page) {
          model.paginate.page = req.query.page;
      }else{
          model.paginate.page = 1;
      }

      // console.log(model);
      // res.ok(model);

      res.render('front/event_records', model);
    }),

    singleData:Async.route(function *(req, res, next) {
      const model = res.model;
      var profile =  yield Organization.where({id: 1}).fetch().catch(console.error);
      model.contact = profile.toJSON();

      var events =  yield EventRecord.where({id: req.params.id}).fetch().catch(console.error);
      model.event = events.toJSON();

      var galleries =  yield EventRecordGalleries.query({ where: { ergal_event_record_id: req.params.id }}).fetch().catch(console.error);
      model.galleries = galleries.toJSON();

      // console.log(model);
      res.render('front/event_record_galleries', model);
    }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
