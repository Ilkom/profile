'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');


module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;

      req.query.limit = 6;
      var paginate = yield pm(News).forge()
      .limit(6)
      .paginate({
          request: req,
      }).query(function(qb) {
          qb.orderBy('id', 'Desc');
      })
      .end();
      model.paginate = paginate;
      if (req.query.page) {
          model.paginate.page = req.query.page;
      }else{
          model.paginate.page = 1;
      }

      var news =  yield Newses.forge().query(function (qb) {
          qb.limit(4);
      }).orderBy('id', 'Desc').fetch().catch(console.error);
      model.newses = news.toJSON();

      labs
      var labs =  yield Labs.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      model.labs = labs.toJSON();
      //
      var profile =  yield Organization.where({id: 1}).fetch().catch(console.error);
      model.contact = profile.toJSON();
      //
      // var news =  yield Newses.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      // model.newses = news.toJSON();
      //
      // res.ok(paginate.toJSON());
      // res.ok(model);
      res.render('front/news', model);
    }),

    singleData: Async.route(function *(req, res, next) {
      const model = res.model;

      // labs
      var labs =  yield Labs.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      model.labs = labs.toJSON();

      var profile =  yield Organization.where({id: 1}).fetch().catch(console.error);
      model.contact = profile.toJSON();

      var news =  yield News.where({id: req.params.id}).fetch().catch(console.error);
      model.news = news.toJSON();
      // console.log(model);
      // res.ok(model);
      res.render('front/news_detail', model);
    }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
