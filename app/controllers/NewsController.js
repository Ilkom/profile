'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

const pathUpload = 'assets/uploads/news';
const pathDb = '/uploads/news/';

module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;
      var user = req.user;
      model.user = user;
      res.render('content/news', model);
    }),

    addNews: Async.route(function *(req, res, next) {
      const model = res.model;
      var user = req.user;
      model.user = user;
      res.render('content/add_news', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(News, 'datatables').forge()
        .paginate({
            request: req,
        })
        .query(function(qb) {
            // qb.where('id', '!=', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    dataSingle: Async.route(function *(req, res, next) {
       const data = res.model;
       var user = req.user;
       data.user = user;

       const beritas =  yield News.where({id: req.params.id}).fetch().catch(console.error);
       data.news = beritas.toJSON();
       console.log(data);

      res.render('content/update_news', data);
   }),


    add: Async.route(function *(req, res, next) {
        req.body.new_cover = '';
        if (!_.isEmpty(req.body.srcDataCrop)) {
            const beritaImagePath = yield upload.base64('news', pathUpload, req.body.srcDataCrop);

            if (!beritaImagePath) throw error('Gagal Menyimpan gambar', 500);
            req.body.new_cover = path.join(pathDb,beritaImagePath);
        }

        delete req.body.srcDataCrop;

        // if your add passHash
        // delete req.body.repassword;
        // var passHash = bcrypt.hashSync(req.body.password, 8);
        // req.body.password = passHash;
        console.log(req.body);

        const news = News.forge();
        const added = yield news.save(req.body).catch(console.error);
        if (_.isEmpty(added)) {
            return res.ok('Failed to add berita');
        }
        return res.ok('1');
    }),

    remove: Async.route(function *(req, res, next) {
        // console.log(req.body);
        if (req.body.newsId.constructor === Array) {
            _.forEach(req.body.newsId, wrap(function *(v, i) {

              var news = yield News.forge({id: v}).fetch().catch(console.error);
              var newsData = news.toJSON();

              const deleted = yield news.destroy().catch(console.error);
                if (!_.isEmpty(deleted)) {
                    return res.ok('delete news failed');
                }
            }));
            return res.ok('1');
        }else {
            var news = yield News.forge({id: req.body.newsId}).fetch().catch(console.error);
            var newsData = news.toJSON();
            const deleted = yield news.destroy().catch(console.error);
            if (_.isEmpty(deleted)) {
              return res.ok('delete news failed');
            }
            return res.ok('1');
        }
    }),

    update: Async.route(function *(req, res, next) {

      if (!_.isEmpty(req.body.srcDataCrop)) {
          const beritaImagePath = yield upload.base64('berita', pathUpload, req.body.srcDataCrop);

          if (!beritaImagePath) throw error('Gagal Menyimpan gambar', 500);
          req.body.new_cover = path.join(pathDb,beritaImagePath);
      }

      delete req.body.srcDataCrop;

       var news = yield News.forge({id: req.params.id}).fetch().catch(console.error);
       const updated = yield news.save(req.body).catch(console.error);
       if (_.isEmpty(updated)) {
           return res.ok('Failed to update Beritas');
       }

       return res.ok('1');
   }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
