'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');


module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;

      // labs
      var labs =  yield Labs.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      model.labs = labs.toJSON();

      var profile =  yield Organization.where({id: 1}).fetch().catch(console.error);
      model.contact = profile.toJSON();
      var courses =  yield Courses.forge().orderBy('cou_semester', 'DESC').fetch().catch(console.error);
      model.courses = courses.toJSON();

      // console.log(model);
      res.render('front/course', model);
    }),

    singleData: Async.route(function *(req, res, next) {
      const model = res.model;

      // labs
      var labs =  yield Labs.forge().orderBy('id', 'Desc').fetch().catch(console.error);
      model.labs = labs.toJSON();
      
      var profile =  yield Organization.where({id: 1}).fetch().catch(console.error);
      model.contact = profile.toJSON();

      var course =  yield Course.where({id: req.params.id}).fetch().catch(console.error);
      model.course = course.toJSON();

      var lcourses =  yield LecturerCourses.query({ where: { lc_course_id: req.params.id }}).fetch({withRelated:['lecturer','lcmaterial']}).catch(console.error);
      model.lcourses = lcourses.toJSON();
      // console.log(model);
      // res.ok(model);
      res.render('front/course_detail', model);
    }),

    lcmData:Async.route(function *(req, res, next) {
      var lcm =  yield LcMaterial.where({lcm_lc_id: req.params.id}).fetch().catch(console.error);
      res.ok(lcm.toJSON());
    }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
