'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

const pathUpload = 'assets/uploads/account';
const pathDb = '/uploads/account/';

module.exports = {

    index: Async.route(function *(req, res, next) {
      const model = res.model;
      var user = req.user;
      model.user = user;

      res.render('content/account', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(Administration, 'datatables').forge()
        .paginate({
            request: req,
        })
        .query(function(qb) {
            // qb.where('id', '!=', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    dataSingle: Async.route(function *(req, res, next) {
      var adm =  yield Administration.where({id: req.params.id}).fetch().catch(console.error);
      res.ok(adm.toJSON());
   }),

    add: Async.route(function *(req, res, next) {
      if (req.body.acc_password == req.body.acc_repass) {
        if (!_.isEmpty(req.body.srcDataCrop)) {
            const accountImagePath = yield upload.base64('account', pathUpload, req.body.srcDataCrop);

            if (!accountImagePath) throw error('Gagal Menyimpan gambar', 500);
            req.body.adm_photo = path.join(pathDb,accountImagePath);
        }

        delete req.body.srcDataCrop;

        // if your add passHash
        delete req.body.repass;
        var passHash = bcrypt.hashSync(req.body.acc_password, 8);
        req.body.acc_password = passHash;
        // req.body.adm_email = req.body.acc_email;

        var admin = {
            'adm_nip' : req.body.adm_nip,
            'adm_name' : req.body.adm_name,
            'adm_nickname' : req.body.adm_nickname,
            'adm_photo' : req.body.adm_photo,
            'adm_gender' : req.body.adm_gender,
            'adm_birthplace' : req.body.adm_birthplace,
            'adm_birthdate' : req.body.adm_birthdate,
            'adm_handphone_number' : req.body.adm_handphone_number,
            'adm_email' : req.body.adm_email,
            'adm_photo' : req.body.adm_photo,
            'adm_address' : req.body.adm_address,
            'adm_city' : req.body.adm_city
        }

        const adm = Administration.forge();
        const added = yield adm.save(admin).catch(console.error);
        if (_.isEmpty(added)) {
            return res.ok('Failed to add administration');
        }

        var account = {
          'acc_email' : added.toJSON().adm_email,
          'acc_password' : req.body.acc_password,
          'acc_user_right' : 2,
        }

        const accounts = Account.forge();
        const added2 = yield accounts.save(account).catch(console.error);
        if (_.isEmpty(added2)) {
            return res.ok('Failed to add account');
        }
        return res.ok('1');
      }else{
        return res.ok('Konfirmasi password tidak sesuai');
      }

    }),

    remove: Async.route(function *(req, res, next) {
        // console.log(req.body);
        if (req.body.administrationId.constructor === Array) {
            _.forEach(req.body.administrationId, wrap(function *(v, i) {

              var adm = yield Administration.forge({id: v}).fetch().catch(console.error);
              var admData = adm.toJSON();

              const deleted1 = yield adm.destroy().catch(console.error);
                if (!_.isEmpty(deleted1)) {
                    return res.ok('delete Administration failed');
                }

              var account = yield Account.forge({id: v}).fetch().catch(console.error);
              var accountData = account.toJSON();

              const deleted = yield account.destroy().catch(console.error);
                if (!_.isEmpty(deleted)) {
                    return res.ok('delete account failed');
                }
            }));
            return res.ok('1');
        }else {

            var adm = yield Administration.forge({id: req.body.administrationId}).fetch().catch(console.error);
            var admData = adm.toJSON();
            const deleted1 = yield adm.destroy().catch(console.error);
            if (_.isEmpty(deleted1)) {
                return res.ok('delete Administration failed');
            }

            var account = yield Account.forge({id: req.body.administrationId}).fetch().catch(console.error);
            var accountData = account.toJSON();
            const deleted = yield account.destroy().catch(console.error);
            if (_.isEmpty(deleted)) {
              return res.ok('delete account failed');
            }
            return res.ok('1');
        }
    }),

    update: Async.route(function *(req, res, next) {
      if (!_.isEmpty(req.body.srcDataCrop)) {
          const accountImagePath = yield upload.base64('account', pathUpload, req.body.srcDataCrop);

          if (!accountImagePath) throw error('Gagal Menyimpan gambar', 500);
          req.body.adm_photo= path.join(pathDb,accountImagePath);
      }

      delete req.body.srcDataCrop;

       var adm = yield Administration.forge({id: req.params.id}).fetch().catch(console.error);
       if (req.body.adm_email) {
         var email = {acc_email:req.body.adm_email};
           var account = yield Account.forge({acc_email: adm.toJSON().adm_email}).fetch().catch(console.error);
           const updatedAcc = yield account.save(email).catch(console.error);
           if (_.isEmpty(updatedAcc)) {
               return res.ok('Failed to update Account');
           }
       }

       const updated = yield adm.save(req.body).catch(console.error);
       if (_.isEmpty(updated)) {
           return res.ok('Failed to update Administrasi');
       }

       return res.ok('1');
   }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
