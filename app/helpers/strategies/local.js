'use strict';
const passport = require('passport');
const Promise = require('bluebird');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(email, password, done) {
        cr(email, password, done).catch(done);
    };
}

function getAccount(email) {
    return Account
        .where({ acc_email: email })
        .fetch()
        .catch(console.error);
}

function getAdministration(email) {
    return Administration
        .where({ adm_email: email })
        .fetch()
        .catch(console.error);
}

function getLeader(email) {
    return Leader
        .where({ lea_email: email })
        .fetch()
        .catch(console.error);
}


const handleAuth = wrap(function *(email, password, done) {
    const account = yield getAccount(email);
    if (_.isEmpty(account)) {
        return done(null, false, { type: 'email', message: 'The email you entered does not belong to any account.' });
    }

    if (!bcrypt.compareSync(password, account.toJSON().acc_password)) {
        return done(null, false, { type: 'password', message: 'The password you entered is incorrect. Please enter again.', email: email });
    }
      if (account.toJSON().acc_user_right == 1) {
          const leader = yield getLeader(email);
          account.toJSON().profile = leader.toJSON();
      }else{
          const administration = yield getAdministration(email);
          account.toJSON().profile = administration.toJSON();
      }
      
    return done(null, account.toJSON());
});

const strategy = new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
}, handleAuth);

passport.use(strategy);
