'use strict';

const passport = require('passport');
const Promise = require('bluebird');

passport.serializeUser((user, done) => {
    // done(null, user);
    done(null, user.id);
});

passport.deserializeUser(wrap(function *(id, done) {
    var user = yield Account.where({id: id}).fetch().catch(console.error);
    if (!_.isEmpty(user)) {
        user = user.toJSON();
        delete user.acc_password;
        if (user.acc_user_right == 1) {
          var leader = yield Leader.where({lea_email: user.acc_email}).fetch().catch(console.error);
          leader = leader.toJSON();
          user.profile = leader;
        }else{
          var admin = yield Administration.where({adm_email: user.acc_email}).fetch().catch(console.error);
          admin = admin.toJSON();
          user.profile = admin;
        }
        done(null, user);
    } else {
        done(new Error('Invalid login data'));
    }
}));

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(id, done) {
        cr(id, done).catch(console.error);
    };
}
