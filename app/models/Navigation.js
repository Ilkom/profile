var navigation = {
    admin: [
        {
            url: '/admin/dashboard',
            displayName: 'Dashboard',
            child: false,
            heading: false,
            icon: 'si-speedometer',
        },
        {
            // url: '#',
            displayName: 'Informasi Publik',
            child: [

              {
                  url: '/admin/news',
                  displayName: 'Berita',
                  child: false,
                  icon: 'si-feed',
              },
              {
                  url: '/admin/event',
                  displayName: 'Event Kegiatan',
                  child: false,
                  heading: false,
                  icon: 'si-feed',
              },
              {
                  url: '/admin/eventrecord',
                  displayName: 'Galeri',
                  child: false,
                  heading: false,
                  icon: 'si-feed',
              },
              {
                  url: '/admin/achievement',
                  displayName: 'Prestasi',
                  child: false,
                  heading: false,
                  icon: 'si-feed',
              },

              {
                  url: '/admin/home',
                  displayName: 'Foto Background',
                  child: false,
                  icon: 'si-feed',
              },
            ],
            // heading:true,
            icon: 'si-feed',
        },
        {
            // url: '#',
            displayName: 'Akun',
            child: [
              {
                  url: '/admin/leaders',
                  displayName: 'Pimpinan Prodi',
                  child: false,
                  heading: false,
                  icon: 'si-user',
              },
              {
                  url: '/admin/administration',
                  displayName: 'Administrasi',
                  child: false,
                  heading: false,
                  icon: 'si-user',
              },
            ],
            // heading:true,
            icon: 'si-users',
        },
        {
            // url: '#',
            displayName: 'Akademik',
            child: [
              {
                  url: '/admin/lecturers',
                  displayName: 'Dosen',
                  child: false,
                  heading: false,
                  icon: 'si-users',
              },
              {
                  url: '/admin/researche',
                  displayName: 'Penelitian',
                  child: false,
                  heading: false,
                  icon: 'si-feed',
              },
              {
                  url: '/admin/labs',
                  displayName: 'Laboratorium',
                  child: false,
                  heading: false,
                  icon: 'si-chemistry',
              },
              {
                  url: '/admin/courses',
                  displayName: 'Matakuliah',
                  child: false,
                  heading: false,
                  icon: 'si-notebook',
              },
              {
                  url: '/admin/instructures',
                  displayName: 'Instruktur Laboratorium',
                  child: false,
                  heading: false,
                  icon: 'si-notebook',
              },
              {
                  url: '/admin/affiliate',
                  displayName: 'Afiliasi',
                  child: false,
                  heading: false,
                  icon: 'si-notebook',
              },
              {
                  url: '/admin/academicYear',
                  displayName: 'Kalender Akademik',
                  child: false,
                  heading: false,
                  icon: 'si-notebook',
              },
            ],
            // heading:true,
            icon: 'si-graduation',
        },

        {
            // url: '#',
            displayName: 'Profil Organisasi',
            child: [
              {
                  url: '/admin/organization',
                  displayName: 'Identitas dan Pengalamatan',
                  child: false,
                  heading: false,
                  icon: 'si-feed',
              },{
                  url: '/admin/profile',
                  displayName: 'Keorganisasi',
                  child: false,
                  heading: false,
                  icon: 'si-feed',
              },
            ],
            // heading:true,
            icon: 'si-user',
        },
    ],
};
global.Navigations = navigation;
