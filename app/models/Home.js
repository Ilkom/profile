'use strict';

const Home = require('./Home');

module.exports = bookshelf.Model.extend({
    tableName: 'homes',
    hasTimestamps: true,
});
