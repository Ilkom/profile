'use strict';

const AcademicCalendar = require('./AcademicCalendar');

module.exports = bookshelf.Model.extend({
    tableName: 'academic_year',
    hasTimestamps: true,
    //
     eventRecordGallery : function() {
        return this.hasMany(AcademicCalendar, 'acc_year_id');
    },

});
