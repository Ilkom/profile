'use strict';

const LecturerCourse = require('./LecturerCourse');
const Lecturer = require('./Lecturer');
const Course = require('./Course');
const LcMaterial = require('./LcMaterial');

module.exports = bookshelf.Model.extend({
    tableName: 'lecturer_courses',
    hasTimestamps: true,

    courses: function(){
      return this.belongsTo( Course, 'lc_course_id');
    },

    lecturer: function() {
      return this.belongsTo( Lecturer, 'lc_lecturer_id');
    },

    lcmaterial: function() {
      return this.hasMany( LcMaterial, 'lcm_lc_id');
    },

});
