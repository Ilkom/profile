'use strict';

const EventRecord = require('./EventRecord');
const EventRecordGallery = require('./EventRecordGallery');

module.exports = bookshelf.Model.extend({
    tableName: 'event_records',
    hasTimestamps: true,
    //
     eventRecordGallery : function() {
        return this.hasMany(EventRecordGallery, 'ergal_event_record_id');
    },

});
