'use strict';

const Profile = require('./Profile');

module.exports = bookshelf.Model.extend({
    tableName: 'profiles',
    hasTimestamps: true,
});
