'use strict';

const Lecturer = require('./Lecturer');
const LecturerCourse = require('./LecturerCourse');

module.exports = bookshelf.Model.extend({
    tableName: 'lecturers',
    hasTimestamps: true,

    lecturerCourse: function() {
      return this.hasMany( LecturerCourse, 'lcm_lc_id');
    },
});
