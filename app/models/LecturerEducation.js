'use strict';

const LecturerEducation = require('./LecturerEducation');

module.exports = bookshelf.Model.extend({
    tableName: 'lecturer_educations',
    hasTimestamps: true,
});
