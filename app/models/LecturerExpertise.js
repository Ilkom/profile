'use strict';

const LecturerExpertise = require('./LecturerExpertise');

module.exports = bookshelf.Model.extend({
    tableName: 'lecturer_expertises',
    hasTimestamps: true,
});
