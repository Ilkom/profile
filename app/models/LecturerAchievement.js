'use strict';

const LecturerAchievement = require('./LecturerAchievement');

module.exports = bookshelf.Model.extend({
    tableName: 'lecturer_achievement',
    hasTimestamps: true,
});
