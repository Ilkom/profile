'use strict';


const AcademicYear = require('./AcademicYear');

module.exports = bookshelf.Model.extend({
    tableName: 'academic_calendars',
    hasTimestamps: true,
    //
     eventRecord : function() {
        return this.belongsTo(AcademicYear, 'id');
    },

});
