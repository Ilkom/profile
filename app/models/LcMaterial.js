'use strict';

const LecturerCourse = require('./LecturerCourse');
const LcMaterial = require('./LcMaterial');

module.exports = bookshelf.Model.extend({
    tableName: 'lc_materials',
    hasTimestamps: true,

    lecturerCourse: function() {
      return this.belongsTo( LecturerCourse, 'id');
    },

});
