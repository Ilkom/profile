'use strict';

const LecturerResearche = require('./LecturerResearche');

module.exports = bookshelf.Model.extend({
    tableName: 'lecturer_researches',
    hasTimestamps: true,
});
