'use strict';

const Affiliate = require('./Affiliate');

module.exports = bookshelf.Model.extend({
    tableName: 'affiliates',
    hasTimestamps: true,

});
