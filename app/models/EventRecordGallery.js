'use strict';

const EventRecordGallery = require('./EventRecordGallery');
const EventRecord = require('./EventRecord');

module.exports = bookshelf.Model.extend({
    tableName: 'event_record_galleries',
    hasTimestamps: true,
    //
     eventRecord : function() {
        return this.belongsTo(EventRecord, 'id');
    },

});
