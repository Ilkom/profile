'use strict';

const Course = require('./Course');
const LecturerCourse = require('./LecturerCourse');

module.exports = bookshelf.Model.extend({
    tableName: 'courses',
    hasTimestamps: true,
    //
    lecturerCourse: function() {
      return this.hasMany( LecturerCourse, 'id');
    },

});
