'use strict';

const LecturerEducation = require('../models/LecturerEducation');

module.exports = bookshelf.Collection.extend({
    model: LecturerEducation,
});
