'use strict';

const EventRecord = require('../models/EventRecord');

module.exports = bookshelf.Collection.extend({
    model: EventRecord,
});
