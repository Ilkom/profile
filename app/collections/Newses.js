'use strict';

const News = require('../models/News');

module.exports = bookshelf.Collection.extend({
    model: News,
});
