'use strict';

const Course = require('../models/Course');

module.exports = bookshelf.Collection.extend({
    model: Course,
});
