'use strict';

const LabGallery = require('../models/LabGallery');

module.exports = bookshelf.Collection.extend({
    model: LabGallery,
});
