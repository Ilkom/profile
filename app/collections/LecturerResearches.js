'use strict';

const LecturerResearche = require('../models/LecturerResearche');

module.exports = bookshelf.Collection.extend({
    model: LecturerResearche,
});
