'use strict';

const AcademicCalendar = require('../models/AcademicCalendar');

module.exports = bookshelf.Collection.extend({
    model: AcademicCalendar,
});
