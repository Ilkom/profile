'use strict';

const Lab = require('../models/Lab');

module.exports = bookshelf.Collection.extend({
    model: Lab,
});
