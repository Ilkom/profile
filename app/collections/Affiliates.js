'use strict';

const Affiliate = require('../models/Affiliate');

module.exports = bookshelf.Collection.extend({
    model: Affiliate,
});
