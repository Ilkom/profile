'use strict';

const Instructure = require('../models/Instructure');

module.exports = bookshelf.Collection.extend({
    model: Instructure,
});
