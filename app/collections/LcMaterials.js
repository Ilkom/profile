'use strict';

const LcMaterial = require('../models/LcMaterial');

module.exports = bookshelf.Collection.extend({
    model: LcMaterial,
});
