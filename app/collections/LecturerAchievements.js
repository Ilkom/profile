'use strict';

const LecturerAchievement = require('../models/LecturerAchievement');

module.exports = bookshelf.Collection.extend({
    model: LecturerAchievement,
});
