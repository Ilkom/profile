'use strict';

const AcademicYear = require('../models/AcademicYear');

module.exports = bookshelf.Collection.extend({
    model: AcademicYear,
});
