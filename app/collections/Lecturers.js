'use strict';

const Lecturer = require('../models/Lecturer');

module.exports = bookshelf.Collection.extend({
    model: Lecturer,
});
