'use strict';

const Account = require('../models/Account');

module.exports = bookshelf.Collection.extend({
    model: Account,
});
