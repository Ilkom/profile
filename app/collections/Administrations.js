'use strict';

const Administration = require('../models/Administration');

module.exports = bookshelf.Collection.extend({
    model: Administration,
});
