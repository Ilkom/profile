'use strict';

const LecturerCourse = require('../models/LecturerCourse');

module.exports = bookshelf.Collection.extend({
    model: LecturerCourse,
});
