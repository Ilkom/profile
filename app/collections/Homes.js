'use strict';

const Home = require('../models/Home');

module.exports = bookshelf.Collection.extend({
    model: Home,
});
