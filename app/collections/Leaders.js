'use strict';

const Leader = require('../models/Leader');

module.exports = bookshelf.Collection.extend({
    model: Leader,
});
