'use strict';

const Profile = require('../models/Profile');

module.exports = bookshelf.Collection.extend({
    model: Profile,
});
