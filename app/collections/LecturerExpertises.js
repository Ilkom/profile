'use strict';

const LecturerExpertise = require('../models/LecturerExpertise');

module.exports = bookshelf.Collection.extend({
    model: LecturerExpertise,
});
