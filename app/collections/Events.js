'use strict';

const Event = require('../models/Event');

module.exports = bookshelf.Collection.extend({
    model: Event,
});
