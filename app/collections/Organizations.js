'use strict';

const Organization = require('../models/Organization');

module.exports = bookshelf.Collection.extend({
    model: Organization,
});
