'use strict';

const EventRecordGallery = require('../models/EventRecordGallery');

module.exports = bookshelf.Collection.extend({
    model: EventRecordGallery,
});
